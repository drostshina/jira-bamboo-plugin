package com.atlassian.jira.plugin.ext.bamboo.web;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.jira.plugin.ext.bamboo.applinks.BambooApplicationLinkManager;
import com.atlassian.jira.web.action.JiraWebActionSupport;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.velocity.htmlsafe.HtmlSafe;
import com.atlassian.webresource.api.assembler.PageBuilderService;
import com.atlassian.webresource.api.assembler.WebResourceAssembler;
import org.apache.velocity.tools.generic.SortTool;

import static com.atlassian.jira.permission.GlobalPermissionKey.ADMINISTER;
import static com.atlassian.jira.plugin.ext.bamboo.applinks.BambooApplicationLinkManager.Filter.UNFILTERED;
import static com.google.common.base.Preconditions.checkNotNull;

@Scanned
public abstract class BambooWebActionSupport extends JiraWebActionSupport {
    public static final String BAMBOO_PLUGIN_KEY = "com.atlassian.jira.plugin.ext.bamboo";

    private final BambooApplicationLinkManager applinkManager;
    private final SortTool sorter = new SortTool();
    private final WebResourceAssembler webResourceAssembler;

    protected BambooWebActionSupport(
            @ComponentImport final PageBuilderService jiraPageBuilderService,
            final BambooApplicationLinkManager applinkManager) {
        this.applinkManager = checkNotNull(applinkManager);
        this.webResourceAssembler = checkNotNull(jiraPageBuilderService).assembler();
    }

    public boolean hasPermissions() {
        return hasGlobalPermission(ADMINISTER);
    }

    public boolean hasMultipleBambooApplinks() {
        return applinkManager.getApplicationLinkCount(UNFILTERED) > 1;
    }

    public boolean hasBambooApplinks() {
        return applinkManager.hasApplicationLinks(UNFILTERED);
    }

    public Iterable<ApplicationLink> getBambooApplinks() {
        return applinkManager.getApplicationLinks(UNFILTERED);
    }

    public boolean hasProjectsAssociatedWithApplink(String applinkId) {
        return applinkManager.hasAssociatedProjects(applinkId);
    }

    public Iterable<String> getProjectsAssociatedWithApplink(String applinkId) {
        return applinkManager.getProjects(applinkId);
    }

    @Override
    public String doDefault() throws Exception {
        return hasPermissions() ? INPUT : PERMISSION_VIOLATION_RESULT;
    }

    @Override
    public String execute() throws Exception {
        if (!hasPermissions()) {
            return PERMISSION_VIOLATION_RESULT;
        }
        webResourceAssembler.resources().requireWebResource(BambooWebActionSupport.BAMBOO_PLUGIN_KEY + ":" + "css");
        return super.execute();
    }

    public SortTool getSorter() {
        return sorter;
    }

    public BambooApplicationLinkManager getApplinkManager() {
        return applinkManager;
    }

    @HtmlSafe
    @Override
    public String getText(String i18nKey) {
        return getI18nHelper().getText(i18nKey);
    }
}
