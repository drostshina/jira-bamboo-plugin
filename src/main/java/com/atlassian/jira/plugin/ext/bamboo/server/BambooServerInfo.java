package com.atlassian.jira.plugin.ext.bamboo.server;

import org.apache.log4j.Logger;
import org.json.JSONObject;

import java.util.Collection;

public class BambooServerInfo {
    private static final Logger log = Logger.getLogger(BambooServerInfo.class);

    private final int buildNumber;
    private final String version;

    public BambooServerInfo(final String version, final int buildNumber) {
        this.version = version;
        this.buildNumber = buildNumber;
    }

    public int getBuildNumber() {
        return buildNumber;
    }

    public String getVersion() {
        return version;
    }

    public static BambooServerInfo fromJSON(final JSONObject jsonObject, final Collection<String> errorCollection) {
        if (jsonObject != null) {
            try {
                String version = jsonObject.getString("version");
                int buildNumber = jsonObject.getInt("buildNumber");
                return new BambooServerInfo(version, buildNumber);
            } catch (Exception e) {
                log.warn("error parsing Bamboo server status", e);
                errorCollection.add("error parsing Bamboo server status: " + e.getMessage());
                return null;
            }
        }
        return null;
    }
}
