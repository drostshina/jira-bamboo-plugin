package com.atlassian.jira.plugin.ext.bamboo.service;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkRequestFactory;
import com.atlassian.applinks.api.CredentialsRequiredException;
import com.atlassian.jira.plugin.ext.bamboo.model.BambooDeploymentProject;
import com.atlassian.jira.plugin.ext.bamboo.model.BambooEnvironment;
import com.atlassian.jira.plugin.ext.bamboo.model.BambooPlan;
import com.atlassian.jira.plugin.ext.bamboo.model.BambooProject;
import com.atlassian.jira.plugin.ext.bamboo.model.PlanKey;
import com.atlassian.jira.plugin.ext.bamboo.model.PlanResultKey;
import com.atlassian.jira.plugin.ext.bamboo.model.PlanResultStatus;
import com.atlassian.jira.plugin.ext.bamboo.model.PlanStatus;
import com.atlassian.jira.plugin.ext.bamboo.model.RestResult;
import com.atlassian.jira.plugin.ext.bamboo.server.BambooServerInfo;
import org.json.JSONObject;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.List;
import java.util.Map;

/**
 * Accessor for the Bamboo REST API. All calls are synchronous and may block.
 */
public interface BambooRestService {
    /**
     * Lists all Projects and Plans on the Bamboo Server represented by {@link ApplicationLink}.
     *
     * @param applicationLink to retrieve plans from
     * @param includeDisabled if false will only retrieve enabled plans, otherwise all will be returned.
     * @return map of {@link BambooProject} to plans {@link BambooPlan}s
     * @throws CredentialsRequiredException if authentication dance required
     */
    @Nonnull
    RestResult<Map<BambooProject, List<BambooPlan>>> getPlanList(
            @Nonnull ApplicationLink applicationLink, boolean includeDisabled)
            throws CredentialsRequiredException;

    /**
     * Get the {@link PlanStatus} for the given {@link PlanKey} at the Bamboo instance using the provided requestFactory.
     *
     * @param applicationLinkRequestFactory the request factory to be used to get PlanStatus
     * @param planKey                       key of the Plan we want to obtain latest status
     * @throws CredentialsRequiredException if authentication dance required
     */
    @Nonnull
    RestResult<PlanStatus> getPlanStatus(
            @Nonnull ApplicationLinkRequestFactory applicationLinkRequestFactory, @Nonnull PlanKey planKey)
            throws CredentialsRequiredException;

    /**
     * Get the {@link PlanResultStatus} for the given {@link PlanResultKey} at the Bamboo instance using the provided requestFactory.
     *
     * @param applicationLinkRequestFactory the request factory to be used to get PlanResultStatus
     * @param planResultKey                 key of the PlanResult we want to obtain status
     * @throws CredentialsRequiredException if authentication dance required
     */
    @Nonnull
    RestResult<PlanResultStatus> getPlanResultStatus(
            @Nonnull ApplicationLinkRequestFactory applicationLinkRequestFactory, @Nonnull PlanResultKey planResultKey)
            throws CredentialsRequiredException;

    /**
     * Directly get the JSON for the given {@link PlanResultKey} at the Bamboo instance using the provide requestFactory.
     *
     * This call adds "?expand=artifacts.artifact,labels.label" to the request
     *
     * @param applicationLinkRequestFactory the request factory to use
     * @param planResultKey                 the plan result key
     * @return jsonObject
     * @throws CredentialsRequiredException if authentication is required.
     */
    @Nonnull
    RestResult<JSONObject> getPlanResultJson(
            @Nonnull ApplicationLinkRequestFactory applicationLinkRequestFactory, @Nonnull PlanResultKey planResultKey)
            throws CredentialsRequiredException;

    /**
     * Directly get the JSON for the given {@link PlanKey} at the Bamboo instance using the provide requestFactory.
     * This call adds "?expand=stages,variableContext" to the request.
     *
     * @param applicationLinkRequestFactory the request factory to use
     * @param planKey                       the plan key
     * @return jsonObject
     * @throws CredentialsRequiredException if authentication is required.
     */
    @Nonnull
    RestResult<JSONObject> getPlanJson(
            @Nonnull ApplicationLinkRequestFactory applicationLinkRequestFactory, @Nonnull PlanKey planKey)
            throws CredentialsRequiredException;

    /**
     * Load the latest results for the given {@link PlanKey}.
     *
     * @param applicationLinkRequestFactory the request factory to use
     * @param planKey                       the plan key
     * @param numberOfResults               to load
     * @param urlParams                     to pass on
     * @return jsonObject
     * @throws CredentialsRequiredException if the caller is not authenticated
     * @throws IllegalArgumentException     if numberOfResults less than or equal to 0
     */
    RestResult<JSONObject> getPlanHistory(@Nonnull ApplicationLinkRequestFactory applicationLinkRequestFactory,
                                          @Nonnull PlanKey planKey, int numberOfResults, @Nonnull Map<String, String> urlParams)
            throws CredentialsRequiredException;

    /**
     * Directly execute the plan at the Bamboo instance represented by {@link ApplicationLink} with optional params.
     *
     * @param applicationLink to the Bamboo instance with the plan.
     * @param planKey         of the plan to execute
     * @param stage           the name of the manual stage (if any) that you want to run automatically. If there is more than one manual stage, only the last one should be specified
     * @param params          to send with the rest request to Bamboo
     * @return RestResult containing planResultKey if successful otherwise any error messages.
     * @throws CredentialsRequiredException if authentication is required.
     */
    @Nonnull
    RestResult<PlanResultKey> triggerPlan(@Nonnull ApplicationLink applicationLink, @Nonnull PlanKey planKey,
                                          @Nullable String stage, @Nonnull Map<String, String> params)
            throws CredentialsRequiredException;

    /**
     * Directly continue the plan at the Bamboo instance represented by {@link ApplicationLink} with optional params.
     *
     * @param applicationLink to the Bamboo instance with the plan.
     * @param planResultKey   of the plan to continue
     * @param stage           the name of the manual stage (if any) that you want to run automatically.  If there is more than one manual stage, only the last one should be specified
     * @param params          to send with the rest request to Bamboo
     * @return RestResult containing planResultKey if successful otherwise any error messages.
     * @throws CredentialsRequiredException if authentication is required.
     */
    @Nonnull
    RestResult<PlanResultKey> continuePlan(@Nonnull ApplicationLink applicationLink, @Nonnull PlanResultKey planResultKey,
                                           @Nullable String stage, @Nonnull Map<String, String> params)
            throws CredentialsRequiredException;

    /**
     * Get JSON describing relation between an issue and specified deployment project.
     *
     * @param applicationLink     the application link
     * @param issueKey            the issue key
     * @param deploymentProjectId the ID of the deployment project
     * @return see above
     * @throws CredentialsRequiredException if authentication is required.
     * @since 6.2
     */
    RestResult<JSONObject> getIssueDeploymentStatus(
            @Nonnull final ApplicationLink applicationLink, @Nonnull String issueKey, @Nonnull String deploymentProjectId)
            throws CredentialsRequiredException;

    /**
     * Get JSON describing relation between an issue and Bamboo deployment projects.
     *
     * @param applicationLinkRequestFactory the request factory to use
     * @param serverName                    the server name
     * @param issueKey                      the issue key
     * @return see above
     * @throws CredentialsRequiredException if authentication is required.
     * @since 6.2
     */
    RestResult<JSONObject> getDeploymentProjectsForIssue(
            @Nonnull ApplicationLinkRequestFactory applicationLinkRequestFactory, @Nonnull String serverName, @Nonnull String issueKey)
            throws CredentialsRequiredException;

    /**
     * Get Bamboo server information.
     *
     * @param applicationLinkRequestFactory the request factory to use
     * @return see above
     * @throws CredentialsRequiredException if authentication is required.
     * @since 6.2
     */
    RestResult<BambooServerInfo> getBambooServerInfo(
            @Nonnull ApplicationLinkRequestFactory applicationLinkRequestFactory)
            throws CredentialsRequiredException;

    /**
     * Lists all Deployment Projects and Environments on the Bamboo Server represented by {@link ApplicationLink}.
     *
     * @param applicationLink to retrieve plans from
     * @return map of {@link BambooDeploymentProject} to plans {@link BambooEnvironment}s
     * @throws CredentialsRequiredException if authentication dance required
     */
    @Nonnull
    RestResult<Map<BambooDeploymentProject, List<BambooEnvironment>>> getEnvironmentList(@Nonnull ApplicationLink applicationLink)
            throws CredentialsRequiredException;
}
