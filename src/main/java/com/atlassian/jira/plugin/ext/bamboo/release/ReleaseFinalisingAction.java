package com.atlassian.jira.plugin.ext.bamboo.release;

import com.atlassian.jira.plugin.ext.bamboo.PluginConstants;
import com.atlassian.jira.plugin.ext.bamboo.model.PlanResultStatus;
import com.atlassian.jira.plugin.ext.bamboo.service.BambooReleaseService;
import com.atlassian.jira.plugin.ext.bamboo.service.PlanResultStatusUpdateService;
import com.atlassian.jira.plugin.ext.bamboo.service.ReleaseErrorReportingService;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.version.Version;
import com.atlassian.jira.project.version.VersionManager;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import org.apache.log4j.Logger;

import java.util.Map;

import static com.google.common.base.Preconditions.checkNotNull;

@Scanned
public class ReleaseFinalisingAction implements PlanResultStatusUpdateService.FinalizingAction {
    private static final Logger log = Logger.getLogger(ReleaseFinalisingAction.class);

    private final BambooReleaseService bambooReleaseService;
    private final I18nHelper i18nHelper;
    private final long versionId;
    private final VersionManager versionManager;
    private final ReleaseErrorReportingService releaseErrorReportingService;

    public ReleaseFinalisingAction(
            final BambooReleaseService bambooReleaseService,
            @ComponentImport final I18nHelper i18nHelper,
            final ReleaseErrorReportingService releaseErrorReportingService,
            final long versionId,
            @ComponentImport final VersionManager versionManager) {
        this.i18nHelper = checkNotNull(i18nHelper);
        this.bambooReleaseService = checkNotNull(bambooReleaseService);
        this.releaseErrorReportingService = checkNotNull(releaseErrorReportingService);
        this.versionId = versionId;
        this.versionManager = checkNotNull(versionManager);
    }

    public void execute(final PlanResultStatus planStatus) {
        final Version version = versionManager.getVersion(versionId);
        final Project project = version.getProject();

        final Map<String, String> buildData = bambooReleaseService.getBuildData(project.getKey(), version.getId());

        // We used to show an error in the UI along with the error logging shown below, but the error message
        // shown to users was ambiguous and not helpful in solving the underlying problem. Users could also sometimes
        // be shown an error message even though there was no underlying problem, due to race conditions in JBAM.
        if (buildData == null || buildData.get(PluginConstants.PS_BUILD_RESULT) == null) {
            log.error("Release build " + planStatus.getPlanResultKey() + " completed but no record of triggering the release can be found.  Version was not released.");
        } else if (!buildData.get(PluginConstants.PS_BUILD_RESULT).equals(planStatus.getPlanResultKey().getKey())) {
            log.error("Release build " + planStatus.getPlanResultKey() + " completed but it does not match the Plan Result we were waiting for (" + buildData.get(PluginConstants.PS_BUILD_RESULT) + ").  Version was not released.");
        } else {
            bambooReleaseService.releaseIfRequired(planStatus, version);
        }
    }
}
