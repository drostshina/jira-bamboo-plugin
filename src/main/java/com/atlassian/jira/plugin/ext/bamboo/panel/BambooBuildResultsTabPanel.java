package com.atlassian.jira.plugin.ext.bamboo.panel;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.permission.ProjectPermissions;
import com.atlassian.jira.plugin.ext.bamboo.applinks.BambooApplicationLinkManager;
import com.atlassian.jira.plugin.issuetabpanel.AbstractIssueTabPanel;
import com.atlassian.jira.plugin.issuetabpanel.IssueAction;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.software.api.conditions.ProjectDevToolsIntegrationFeatureCondition;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.atlassian.jira.software.api.conditions.ProjectDevToolsIntegrationFeatureCondition.CONTEXT_KEY_PROJECT;
import static com.google.common.base.Preconditions.checkNotNull;

@Scanned
public class BambooBuildResultsTabPanel extends AbstractIssueTabPanel {
    public static final String VIEW_BUILD_RESULTS_ACTION_URL = "/ajax/build/viewBuildResultsByJiraKey.action";
    public static final String VIEW_PLANS_ACTION_URL = "/ajax/build/viewPlansByJiraKey.action";

    private final BambooApplicationLinkManager bambooApplicationLinkManager;
    private final BambooPanelHelper bambooPanelHelper;
    private final PermissionManager permissionManager;
    private final ProjectDevToolsIntegrationFeatureCondition projectDevToolsIntegrationFeatureCondition;

    public BambooBuildResultsTabPanel(
            @ComponentImport final PermissionManager permissionManager,
            final BambooApplicationLinkManager bambooApplicationLinkManager,
            final BambooPanelHelper bambooPanelHelper,
            final ProjectDevToolsIntegrationFeatureCondition projectDevToolsIntegrationFeatureCondition) {
        this.bambooApplicationLinkManager = checkNotNull(bambooApplicationLinkManager);
        this.bambooPanelHelper = checkNotNull(bambooPanelHelper);
        this.permissionManager = checkNotNull(permissionManager);
        this.projectDevToolsIntegrationFeatureCondition = checkNotNull(projectDevToolsIntegrationFeatureCondition);
    }

    public List<IssueAction> getActions(final Issue issue, final ApplicationUser remoteUser) {
        return Collections.<IssueAction>singletonList(new BambooBuildResultsAction(bambooPanelHelper, issue, descriptor));
    }

    public boolean showPanel(final Issue issue, final ApplicationUser remoteUser) {
        final Map<String, Object> projectContext = new HashMap<>();
        projectContext.put(CONTEXT_KEY_PROJECT, issue.getProjectObject());
        return bambooApplicationLinkManager.hasValidApplicationLinkForIssueBuildPanel(issue.getProjectObject().getKey())
                && permissionManager.hasPermission(ProjectPermissions.VIEW_DEV_TOOLS, issue, remoteUser)
                && projectDevToolsIntegrationFeatureCondition.shouldDisplay(projectContext);
    }
}
