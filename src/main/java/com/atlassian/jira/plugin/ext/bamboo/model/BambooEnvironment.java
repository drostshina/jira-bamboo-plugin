package com.atlassian.jira.plugin.ext.bamboo.model;

import org.apache.commons.lang.builder.CompareToBuilder;

public class BambooEnvironment implements Comparable<BambooEnvironment> {
    private final long environmentId;
    private final String environmentKey;
    private final String environmentName;

    public BambooEnvironment(final long environmentId, final String environmentKey, final String environmentName) {
        this.environmentId = environmentId;
        this.environmentKey = environmentKey;
        this.environmentName = environmentName;
    }

    public long getEnvironmentId() {
        return environmentId;
    }

    public String getEnvironmentKey() {
        return environmentKey;
    }

    public String getEnvironmentName() {
        return environmentName;
    }

    public int compareTo(BambooEnvironment o) {
        return new CompareToBuilder()
                .append(environmentId, o.environmentId)
                .toComparison();
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        final BambooEnvironment that = (BambooEnvironment) o;

        if (environmentId != that.environmentId) {
            return false;
        }
        if (!environmentKey.equals(that.environmentKey)) {
            return false;
        }
        if (!environmentName.equals(that.environmentName)) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (environmentId ^ (environmentId >>> 32));
        result = 31 * result + environmentKey.hashCode();
        result = 31 * result + environmentName.hashCode();
        return result;
    }
}
