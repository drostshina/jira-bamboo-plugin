package com.atlassian.jira.plugin.ext.bamboo.service;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkRequestFactory;
import com.atlassian.applinks.api.CredentialsRequiredException;
import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.event.ClearCacheEvent;
import com.atlassian.jira.plugin.ext.bamboo.applinks.BambooApplicationLinkManager;
import com.atlassian.jira.plugin.ext.bamboo.model.LifeCycleState;
import com.atlassian.jira.plugin.ext.bamboo.model.PlanResultKey;
import com.atlassian.jira.plugin.ext.bamboo.model.PlanResultStatus;
import com.atlassian.jira.plugin.ext.bamboo.model.RestResult;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.project.version.Version;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.I18nHelper;
import com.atlassian.plugin.spring.scanner.annotation.export.ExportAsService;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.lifecycle.LifecycleAware;
import com.atlassian.scheduler.SchedulerService;
import com.atlassian.scheduler.SchedulerServiceException;
import com.atlassian.scheduler.config.JobConfig;
import com.atlassian.scheduler.config.Schedule;
import com.google.common.collect.MapMaker;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.Date;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.Future;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import static com.atlassian.jira.plugin.ext.bamboo.PluginConstants.BAMBOO_ERROR_CONNECTIVITY_I18N_KEY;
import static com.atlassian.jira.plugin.ext.bamboo.PluginConstants.DEFAULT_SERVER_NAME;
import static com.atlassian.scheduler.config.RunMode.RUN_ONCE_PER_CLUSTER;
import static com.atlassian.util.concurrent.ThreadFactories.namedThreadFactory;
import static com.google.common.base.Preconditions.checkNotNull;
import static java.util.concurrent.TimeUnit.MILLISECONDS;
import static java.util.concurrent.TimeUnit.SECONDS;
import static org.apache.commons.lang3.builder.ToStringBuilder.reflectionToString;
import static org.apache.commons.lang3.builder.ToStringStyle.SHORT_PREFIX_STYLE;

@ExportAsService({PlanResultStatusUpdateService.class, LifecycleAware.class})
@Component
public class PlanStatusUpdateServiceImpl
        implements PlanResultStatusUpdateService, LifecycleAware, InitializingBean, DisposableBean {
    private static final Logger log = LoggerFactory.getLogger(PlanStatusUpdateServiceImpl.class);

    private static final long DEFAULT_INTERVAL = TimeUnit.MINUTES.toMillis(1);

    /**
     * System property for changing interval
     */
    private static final String BAMBOO_STATUS_UPDATE_INTERVAL = "bamboo.status.update.interval";

    private final ThreadPoolExecutor threadPoolExecutor = new ThreadPoolExecutor(
            0, 10, 30, SECONDS, new LinkedBlockingQueue<>(), namedThreadFactory("Bamboo Status Update"));

    private final ConcurrentMap<PlanResultKey, Subscription> planResultKeyToSubscriptionMap = new MapMaker().makeMap();
    private final ConcurrentMap<Subscription, Future<?>> subscriptionFutureMap = new MapMaker().makeMap();

    private final BambooApplicationLinkManager bambooApplicationLinkManager;
    private final BambooRestService bambooRestService;
    private final EventPublisher eventPublisher;
    private final ImpersonationService impersonationService;
    private final JiraAuthenticationContext authenticationContext;
    private final ProjectManager projectManager;
    private final ReleaseErrorReportingService releaseErrorReportingService;
    private final SchedulerService schedulerService;

    @Autowired
    public PlanStatusUpdateServiceImpl(
            @ComponentImport final EventPublisher eventPublisher,
            @ComponentImport final JiraAuthenticationContext authenticationContext,
            @ComponentImport final ProjectManager projectManager,
            @ComponentImport final SchedulerService schedulerService,
            final BambooApplicationLinkManager bambooApplicationLinkManager,
            final BambooRestService bambooRestService,
            final ImpersonationService impersonationService,
            final ReleaseErrorReportingService releaseErrorReportingService) {
        this.authenticationContext = checkNotNull(authenticationContext);
        this.bambooApplicationLinkManager = checkNotNull(bambooApplicationLinkManager);
        this.bambooRestService = checkNotNull(bambooRestService);
        this.eventPublisher = checkNotNull(eventPublisher);
        this.impersonationService = checkNotNull(impersonationService);
        this.projectManager = checkNotNull(projectManager);
        this.releaseErrorReportingService = checkNotNull(releaseErrorReportingService);
        this.schedulerService = checkNotNull(schedulerService);
    }

    public void subscribe(@Nonnull final Version version, final @Nonnull PlanResultKey planResultKey,
                          final @Nonnull String username, final @Nonnull FinalizingAction finalizingAction) {
        final Project project = version.getProject();
        log.info("Bamboo Release Plugin waiting for result for build '" + planResultKey + "' for project '" +
                project.getKey() + "' and version '" + version.getName() + "'");
        final Subscription subscription =
                new Subscription(planResultKey, project.getKey(), version.getId(), username, finalizingAction);
        this.planResultKeyToSubscriptionMap.putIfAbsent(planResultKey, subscription);
        scheduleUpdate(subscription);
    }

    public void unsubscribe(@Nonnull final PlanResultKey planResultKey) {
        final Subscription subscription = planResultKeyToSubscriptionMap.remove(planResultKey);
        if (subscription != null) {
            removeFuture(subscription);
        }
    }

    public int scheduleUpdates() {
        int updatesScheduled = 0;
        for (final Subscription subscription : planResultKeyToSubscriptionMap.values()) {
            if (!subscriptionFutureMap.containsKey(subscription)) {
                scheduleUpdate(subscription);
                updatesScheduled++;
                log.debug("Scheduled updating of {}", subscription);
            }
        }
        return updatesScheduled;
    }

    public void onStart() {
        schedulerService.registerJobRunner(PlanStatusUpdateJob.KEY, new PlanStatusUpdateJob(this));

        final long intervalInMillis = getPlanStatusUpdateJobIntervalInMillis();
        final JobConfig jobConfig = JobConfig
                .forJobRunnerKey(PlanStatusUpdateJob.KEY)
                .withRunMode(RUN_ONCE_PER_CLUSTER)
                .withSchedule(Schedule.forInterval(intervalInMillis, new Date()));
        try {
            schedulerService.scheduleJobWithGeneratedId(jobConfig);
            log.info("{} scheduled to run every {} seconds",
                    PlanStatusUpdateJob.class.getSimpleName(), MILLISECONDS.toSeconds(intervalInMillis));
        } catch (final SchedulerServiceException e) {
            log.error("Could not schedule plan status update job");
            throw new RuntimeException(e);
        }
    }

    private long getPlanStatusUpdateJobIntervalInMillis() {
        return Long.getLong(BAMBOO_STATUS_UPDATE_INTERVAL, DEFAULT_INTERVAL);
    }

    public void onStop() {
        schedulerService.unregisterJobRunner(PlanStatusUpdateJob.KEY);
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        eventPublisher.register(this);
    }

    @Override
    public void destroy() throws Exception {
        eventPublisher.unregister(this);
    }

    @EventListener
    @SuppressWarnings("unused")
    public void onEvent(final ClearCacheEvent event) {
        planResultKeyToSubscriptionMap.clear();
        subscriptionFutureMap.clear();
    }

    private void scheduleUpdate(@Nonnull Subscription subscription) {
        final ApplicationLink applicationLink = subscription.getApplicationLink();
        if (applicationLink == null) {
            applicationLinkUnavailable(subscription);
        } else {
            log.info("Scheduling update from '" + applicationLink + "' for Plan '" + subscription.planResultKey + "'");
            final Future<?> planStatusFuture = threadPoolExecutor.submit(subscription.planStatusRunnable);
            subscriptionFutureMap.putIfAbsent(subscription, planStatusFuture);
        }
    }

    /**
     * Report that the application link was unavailable
     *
     * @param subscription the subscription in play
     */
    private void applicationLinkUnavailable(@Nonnull final Subscription subscription) {
        releaseErrorReportingService.recordError(subscription.projectKey, subscription.versionId,
                getI18nHelper().getText(BAMBOO_ERROR_CONNECTIVITY_I18N_KEY, DEFAULT_SERVER_NAME));

        unsubscribe(subscription.planResultKey);

        log.error("ApplicationLink failure - Could not connect to Plan '" + subscription.planResultKey.getPlanKey() +
                "' for Project '" + subscription.projectKey + "'");
    }

    private void removeFuture(final Subscription subscription) {
        subscriptionFutureMap.remove(subscription);
    }

    private final class Subscription {
        private final long versionId;
        private final FinalizingAction finalizingAction;
        private final PlanResultKey planResultKey;
        private final Runnable planStatusRunnable;
        private final String projectKey;

        private Subscription(
                @Nonnull final PlanResultKey planResultKey,
                @Nonnull final String projectKey,
                final long versionId,
                @Nonnull final String username,
                @Nonnull final FinalizingAction finalizingAction) {
            this.finalizingAction = finalizingAction;
            this.planResultKey = planResultKey;
            this.projectKey = projectKey;
            this.versionId = versionId;
            this.planStatusRunnable = impersonationService.runAsUser(username, new UpdatePlanStatus(this));
        }

        @Nullable
        public ApplicationLink getApplicationLink() {
            final Project project = projectManager.getProjectObjByKey(projectKey);
            if (project != null) {
                return bambooApplicationLinkManager.getApplicationLink(project.getKey());
            }
            log.error("Could not find project '" + projectKey + "'");
            return null;
        }

        @Override
        public String toString() {
            return reflectionToString(this, SHORT_PREFIX_STYLE);
        }
    }

    private I18nHelper getI18nHelper() {
        return authenticationContext.getI18nHelper();
    }

    private final class UpdatePlanStatus implements Runnable {
        private final Subscription subscription;

        private UpdatePlanStatus(@Nonnull Subscription subscription) {
            this.subscription = subscription;
        }

        public void run() {
            final ApplicationLink applicationLink = subscription.getApplicationLink();

            if (applicationLink != null) {
                final ApplicationLinkRequestFactory authenticatedRequestFactory = applicationLink.createAuthenticatedRequestFactory();
                if (authenticatedRequestFactory != null) {
                    try {
                        final RestResult<PlanResultStatus> restResult = bambooRestService.getPlanResultStatus(
                                authenticatedRequestFactory, subscription.planResultKey);
                        final PlanResultStatus planStatus = restResult.getResult();

                        if (!restResult.getErrors().isEmpty()) {
                            releaseErrorReportingService.recordError(subscription.projectKey, subscription.versionId,
                                    getI18nHelper().getText(BAMBOO_ERROR_CONNECTIVITY_I18N_KEY, applicationLink.getName()));
                        }

                        if (planStatus != null) {
                            runFinalizingAction(planStatus, subscription);
                        }
                    } catch (CredentialsRequiredException e) {
                        if (log.isDebugEnabled()) {
                            log.debug("Authorisation Required", e);
                        }
                        log.info("JIRA could not connect to the Bamboo instance '" + applicationLink.getName() +
                                "' to complete the release. This may require user authentication");
                    }
                } else {
                    applicationLinkUnavailable(subscription);
                }
            } else {
                applicationLinkUnavailable(subscription);
            }
        }

        private void runFinalizingAction(@Nonnull PlanResultStatus status, @Nonnull Subscription subscription) {
            final PlanResultKey planResultKey = subscription.planResultKey;
            final ApplicationLink applicationLink = subscription.getApplicationLink();

            if (applicationLink == null) {
                applicationLinkUnavailable(subscription);
            } else {
                log.info("Plan '" + status.getPlanResultKey() + "' BuildState: '" + status.getBuildState() +
                        "' LifeCycleState: '" + status.getLifeCycleState() + "'");

                if (status.isValid() && LifeCycleState.isFinalized(status.getLifeCycleState())) {
                    log.info("Bamboo Release Plugin detected that " + planResultKey + " from " +
                            applicationLink.getDisplayUrl() + " has finished.");
                    try {
                        subscription.finalizingAction.execute(status);
                    } catch (Throwable e) {
                        log.error("Could not run action for subscription '" + planResultKey + "' for Bamboo Server '" +
                                applicationLink + "'", e);
                    } finally {
                        unsubscribe(subscription.planResultKey);
                    }
                } else if (!status.isRecoverable()) {
                    unsubscribe(subscription.planResultKey);
                    log.error("Status update reached an unrecoverable state for '" + planResultKey +
                            "' from Bamboo Server '" + applicationLink + "'. JIRA will no longer update the status.");
                }

                log.info("Removing subscription to '" + applicationLink + "' for Plan '" + subscription.planResultKey + "'");
                removeFuture(subscription);
            }
        }
    }
}
