package com.atlassian.jira.plugin.ext.bamboo.view;

import com.atlassian.crowd.embedded.api.User;
import com.atlassian.jira.plugin.ext.bamboo.panel.BambooPanelHelper;
import com.atlassian.jira.plugin.ext.bamboo.service.BambooReleaseService;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.version.Version;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.user.ApplicationUsers;
import com.atlassian.jira.web.ExecutingHttpRequest;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.plugin.web.ContextProvider;
import com.atlassian.soy.renderer.SoyTemplateRenderer;

import java.util.HashMap;
import java.util.Map;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Adds the button to the release report
 */
@Scanned
public class BambooReleaseReportButtonContextProvider implements ContextProvider {
    // We do not have to set the module key, as we never have any sub tabs.
    private static final String MODULE_KEY = "";

    private final SoyTemplateRenderer soyTemplateRenderer;
    private final BambooPanelHelper bambooPanelHelper;
    private final BambooReleaseService bambooReleaseService;

    public BambooReleaseReportButtonContextProvider(@ComponentImport SoyTemplateRenderer soyTemplateRenderer,
                                                    BambooPanelHelper bambooPanelHelper,
                                                    BambooReleaseService bambooReleaseService) {
        this.soyTemplateRenderer = checkNotNull(soyTemplateRenderer);
        this.bambooPanelHelper = checkNotNull(bambooPanelHelper);
        this.bambooReleaseService = checkNotNull(bambooReleaseService);
    }

    @Override
    public void init(Map<String, String> params) {
    }

    @Override
    public Map<String, Object> getContextMap(Map<String, Object> context) {
        final User user = ApplicationUsers.toDirectoryUser((ApplicationUser) context.get("user"));
        final Project project = (Project) context.get("project");
        final Version version = (Version) context.get("version");

        final String baseLinkUrl = getBaseLinkUrl(project, version);
        final String queryString = "versionId=" + version.getId();

        // This needs to happen before the release panel is shown.
        bambooReleaseService.resetReleaseStateIfVersionWasUnreleased(version);

        Map<String, Object> velocityParams = new HashMap<String, Object>(context);

        velocityParams.put("req", ExecutingHttpRequest.get());
        velocityParams.put("soyRenderer", soyTemplateRenderer);

        velocityParams.putAll(bambooPanelHelper.getReleaseButtonParams(ApplicationUsers.from(user), version, project));

        bambooPanelHelper.prepareVelocityContext(velocityParams, MODULE_KEY, baseLinkUrl, queryString, project);

        return velocityParams;
    }

    private String getBaseLinkUrl(Project project, Version version) {
        return "/browse/" + project.getKey() + "/fixforversion/" + version.getId();
    }
}
