package com.atlassian.jira.plugin.ext.bamboo.applinks;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkService;
import com.atlassian.applinks.api.TypeNotInstalledException;
import com.atlassian.applinks.api.application.bamboo.BambooApplicationType;
import com.atlassian.applinks.api.event.ApplicationLinksIDChangedEvent;
import com.atlassian.event.api.EventListener;
import com.atlassian.event.api.EventPublisher;
import com.atlassian.jira.config.FeatureManager;
import com.atlassian.jira.event.ClearCacheEvent;
import com.atlassian.jira.plugin.devstatus.api.DevStatusSupportedApplicationLinkService;
import com.atlassian.jira.plugin.ext.bamboo.optionaldep.DevStatusSupportedAppLinksServiceAccessor;
import com.atlassian.jira.util.BuildUtilsInfo;
import com.atlassian.plugin.PluginAccessor;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.plugins.capabilities.api.LinkedAppWithCapabilities;
import com.atlassian.plugins.capabilities.api.LinkedApplicationCapabilities;
import com.atlassian.util.concurrent.ResettableLazyReference;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.base.Predicates.not;
import static com.google.common.base.Predicates.or;
import static com.google.common.collect.Iterables.filter;

@Component
public class BambooApplicationLinkManagerImpl implements BambooApplicationLinkManager, InitializingBean, DisposableBean {
    @VisibleForTesting
    public static final String ENFORCE_JBAM_FEATURE = "jira.bamboo.legacy.force";

    static final String JBAM_ASSOCIATIONS = "jbam-associations";
    static final String DEV_STATUS_DETAIL_DEPLOYMENT_ENVIRONMENT_CAPABILITY = "dev-status-detail-deployment-environment";

    private static final int JIRA_6_2_BUILD_NUMBER = 6200;
    private static final Logger log = Logger.getLogger(BambooApplicationLinkManagerImpl.class);
    private static final String FUSION_PLUGIN_KEY = "com.atlassian.jira.plugins.jira-development-integration-plugin";

    private final ApplicationLinkService applicationLinkService;
    private final EventPublisher eventPublisher;
    private final PluginAccessor pluginAccessor;
    private final LinkedApplicationCapabilities linkedApplicationCapabilities;
    private final BuildUtilsInfo jiraBuildInfo;
    private final FeatureManager featureManager;
    private final DevStatusSupportedAppLinksServiceAccessor devStatusSupportedAppLinksServiceAccessor;

    private final ResettableLazyReference<Map<String, ApplicationId>> projectApplicationLinks = new ResettableLazyReference<Map<String, ApplicationId>>() {
        @Override
        protected Map<String, ApplicationId> create() {
            //ImmutableMap.Builder<String, ApplicationId> map = ImmutableMap.builder();
            Map<String, ApplicationId> map = new HashMap<String, ApplicationId>();
            for (ApplicationLink applink : getApplicationLinks(Filter.UNFILTERED)) {
                Object projectKeys = applink.getProperty(JBAM_ASSOCIATIONS);
                if (projectKeys != null) {
                    List<String> projectKeyList = (List<String>) projectKeys;
                    for (String projectKey : projectKeyList) {
                        map.put(projectKey, applink.getId());
                    }
                }
            }
            //return map.build();
            return map;
        }
    };

    private class IsApplicationLinkIdEqual implements Predicate<LinkedAppWithCapabilities> {
        private final ApplicationLink applicationLink;

        private IsApplicationLinkIdEqual(@Nonnull final ApplicationLink applicationLink) {
            this.applicationLink = applicationLink;
        }

        @Override
        public boolean apply(@Nullable final LinkedAppWithCapabilities input) {
            return input != null && input.getApplicationLinkId().equals(applicationLink.getId().toString());
        }
    }

    private final Predicate<ApplicationLink> hasDevStatusSummaryCapability = new Predicate<ApplicationLink>() {
        @Override
        public boolean apply(@Nullable final ApplicationLink input) {
            DevStatusSupportedApplicationLinkService devStatusSupportedApplicationLinkService = devStatusSupportedAppLinksServiceAccessor.getService();
            return input != null
                    && devStatusSupportedApplicationLinkService != null
                    && devStatusSupportedApplicationLinkService.hasApplicationLink2LOSupport(input);
        }
    };

    private final Predicate<ApplicationLink> hasDeploymentDetailsCapability = new Predicate<ApplicationLink>() {
        @Override
        public boolean apply(@Nullable final ApplicationLink input) {
            Set<? extends LinkedAppWithCapabilities> capable = linkedApplicationCapabilities.capableOf(DEV_STATUS_DETAIL_DEPLOYMENT_ENVIRONMENT_CAPABILITY);
            return input != null && Iterables.any(capable, new IsApplicationLinkIdEqual(input));
        }
    };

    @Autowired
    public BambooApplicationLinkManagerImpl(
            // Because the MutatingApplicationLinkService is injected elsewhere, we use this
            // qualifier to exclude it and thereby avoid the "No unique bean of type" error.
            @ComponentImport @Qualifier("applicationLinkService") final ApplicationLinkService applicationLinkService,
            @ComponentImport final BuildUtilsInfo jiraBuildInfo,
            @ComponentImport final EventPublisher eventPublisher,
            @ComponentImport final FeatureManager featureManager,
            @ComponentImport final LinkedApplicationCapabilities linkedApplicationCapabilities,
            @ComponentImport final PluginAccessor pluginAccessor,
            final DevStatusSupportedAppLinksServiceAccessor devStatusSupportedAppLinksServiceAccessor) {
        this.applicationLinkService = checkNotNull(applicationLinkService);
        this.devStatusSupportedAppLinksServiceAccessor = checkNotNull(devStatusSupportedAppLinksServiceAccessor);
        this.eventPublisher = checkNotNull(eventPublisher);
        this.featureManager = checkNotNull(featureManager);
        this.jiraBuildInfo = checkNotNull(jiraBuildInfo);
        this.linkedApplicationCapabilities = checkNotNull(linkedApplicationCapabilities);
        this.pluginAccessor = checkNotNull(pluginAccessor);
    }

    @Override
    public boolean hasApplicationLinks(@Nonnull Filter filter) {
        return !Iterables.isEmpty(getApplicationLinks(filter));
    }

    @Override
    public Iterable<ApplicationLink> getApplicationLinks(@Nonnull Filter filter) {
        final Iterable<ApplicationLink> unfiltered = applicationLinkService.getApplicationLinks(BambooApplicationType.class);
        if (shouldFilterBambooApplinks()) {
            switch (filter) {
                case SKIP_LINKS_WITH_BUILD_SUMMARY_CAPABILITY_IF_FUSION_ENABLED:
                    //if bamboo has dev-status-summary it surely has builds capability as well (both appeared in Bamboo 5.3)
                    //but we need to check summary capability, since in Bamboo 5.3 it can be switched off
                    return filter(unfiltered, not(hasDevStatusSummaryCapability));
                case SKIP_LINKS_WITH_DEPLOYMENT_SUMMARY_CAPABILITY_IF_FUSION_ENABLED:
                    //deployment data is only provided by Bamboo 5.4 and up and then option to switch dev-status-summary off is gone
                    //so it's sufficient to check deployment capability
                    return filter(unfiltered, or(not(hasDevStatusSummaryCapability), not(hasDeploymentDetailsCapability)));
            }
        }
        return unfiltered;
    }

    @Override
    public int getApplicationLinkCount(@Nonnull Filter filter) {
        return Iterables.size(getApplicationLinks(filter));
    }

    public ApplicationLink getApplicationLink(String projectKey) {
        Map<String, ApplicationId> applicationIdMap = projectApplicationLinks.get();
        if (applicationIdMap.containsKey(projectKey)) {
            ApplicationId appId = applicationIdMap.get(projectKey);

            try {
                return applicationLinkService.getApplicationLink(appId);
            } catch (TypeNotInstalledException e) {
                log.warn("Application link cannot be found for project '" + projectKey + "' and applicationId '" + appId + "'.");
            }
        }

        return getDefaultApplicationLink();
    }

    @Override
    public boolean hasValidApplicationLinkForIssueBuildPanel(final String projectKey) {
        ApplicationLink applicationLink = getApplicationLink(projectKey);
        return applicationLink != null && (!shouldFilterBambooApplinks() || !hasDevStatusSummaryCapability.apply(applicationLink));
    }

    public ApplicationLink getBambooApplicationLink(String appId) {
        try {
            ApplicationLink applicationLink = applicationLinkService.getApplicationLink(new ApplicationId(appId));
            if (!(BambooApplicationType.class.isAssignableFrom(applicationLink.getType().getClass()))) {
                //only return Bamboo application links
                return null;
            }

            return applicationLink;
        } catch (TypeNotInstalledException e) {
            //application link doesn't exist. return null.
            return null;
        }

    }

    public Iterable<String> getProjects(String appId) {
        ApplicationLink applink = lookupApplicationLink(new ApplicationId(appId));
        final Object projectKeys = applink.getProperty(JBAM_ASSOCIATIONS);
        if (projectKeys == null) {
            return ImmutableList.of();
        } else {
            return (List<String>) projectKeys;
        }
    }

    public boolean hasAssociatedProjects(String appId) {
        return !Iterables.isEmpty(getProjects(appId));
    }

    public boolean hasAssociatedApplicationLink(String projectKey) {
        return projectApplicationLinks.get().containsKey(projectKey);
    }

    public boolean isAssociated(String projectKey, ApplicationId applicationId) {
        Map<String, ApplicationId> applicationIdMap = projectApplicationLinks.get();
        return applicationIdMap.containsKey(projectKey) && applicationIdMap.get(projectKey).equals(applicationId);
    }

    public void associate(String projectKey, ApplicationId appId) {
        ApplicationLink applink = lookupApplicationLink(appId);
        final Object projectKeys = applink.getProperty(JBAM_ASSOCIATIONS);
        if (projectKeys == null) {
            applink.putProperty(JBAM_ASSOCIATIONS, ImmutableList.of(projectKey));
        } else {
            List<String> projectKeysList = (List<String>) projectKeys;
            applink.putProperty(JBAM_ASSOCIATIONS, ImmutableList.builder().addAll(projectKeysList).add(projectKey).build());
        }

        projectApplicationLinks.reset();

        log.info("Associated project '" + projectKey + "' with application link '" + appId + "'.");
    }

    public void unassociateAll(ApplicationId appId) {
        ApplicationLink applink = lookupApplicationLink(appId);
        Object projectKeys = applink.getProperty(JBAM_ASSOCIATIONS);
        if (projectKeys != null) {
            projectApplicationLinks.reset();
            applink.removeProperty(JBAM_ASSOCIATIONS);
        }

        log.info("Unassociated all projects with application link '" + appId + "'.");
    }

    @EventListener
    public void onEvent(ClearCacheEvent event) {
        projectApplicationLinks.reset();
    }

    @EventListener
    public void onEvent(ApplicationLinksIDChangedEvent event) {
        Object associations = event.getApplicationLink().getProperty(JBAM_ASSOCIATIONS);

        //if we have any applinks set up for these project, update the references
        if (associations != null) {
            projectApplicationLinks.reset();
        }
    }

    private ApplicationLink lookupApplicationLink(ApplicationId appId) {
        try {
            return applicationLinkService.getApplicationLink(appId);
        } catch (TypeNotInstalledException e) {
            throw new IllegalArgumentException(e);
        }
    }

    /**
     * Called upon startup.
     */
    public void afterPropertiesSet() throws Exception {
        eventPublisher.register(this);
    }

    /**
     * Called upon shutdown.
     */
    public void destroy() throws Exception {
        eventPublisher.unregister(this);
    }

    private ApplicationLink getDefaultApplicationLink() {
        return applicationLinkService.getPrimaryApplicationLink(BambooApplicationType.class);
    }

    protected boolean shouldFilterBambooApplinks() {
        return (jiraBuildInfo.getApplicationBuildNumber() >= JIRA_6_2_BUILD_NUMBER
                && pluginAccessor.getEnabledPlugin(FUSION_PLUGIN_KEY) != null
                && !featureManager.getDarkFeatures().isFeatureEnabled(ENFORCE_JBAM_FEATURE));
    }
}
