package com.atlassian.jira.plugin.ext.bamboo.panel;

import com.atlassian.jira.plugin.ext.bamboo.applinks.BambooApplicationLinkManager;
import com.atlassian.jira.plugin.projectpanel.impl.AbstractProjectTabPanel;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.browse.BrowseContext;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.software.api.conditions.ProjectDevToolsIntegrationFeatureCondition;
import com.atlassian.jira.web.FieldVisibilityManager;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.google.common.annotations.VisibleForTesting;
import com.google.common.collect.ImmutableMap;

import java.util.Map;

import static com.atlassian.jira.permission.ProjectPermissions.VIEW_DEV_TOOLS;
import static com.atlassian.jira.plugin.ext.bamboo.applinks.BambooApplicationLinkManager.Filter.UNFILTERED;
import static com.atlassian.jira.plugin.ext.bamboo.panel.BambooPanelHelper.BAMBOO_PLUGIN_KEY;
import static com.atlassian.jira.software.api.conditions.ProjectDevToolsIntegrationFeatureCondition.CONTEXT_KEY_PROJECT;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Maps.newHashMap;

@Scanned
public class BuildsForProjectTabPanel extends AbstractProjectTabPanel {
    @VisibleForTesting
    static final String CONTEXT_KEY_CONTEXT_TYPE = "contextType";

    @VisibleForTesting
    static final String CONTEXT_KEY_USER = "user";

    @VisibleForTesting
    static final String MODEL_KEY_FIELD_VISIBILITY = "fieldVisibility";

    @VisibleForTesting
    static final String MODEL_KEY_PORTLET = "portlet";

    @VisibleForTesting
    static final String PROJECT_CONTEXT_TYPE = "project";

    @VisibleForTesting
    static final String VIEW_RESOURCE = "view";

    private static final String BAMBOO_PLUGIN_MODULE_KEY = "bamboo-project-tabpanel";

    private final BambooApplicationLinkManager bambooApplicationLinkManager;
    private final BambooPanelHelper bambooPanelHelper;
    private final FieldVisibilityManager fieldVisibilityManager;
    private final PermissionManager permissionManager;
    private final ProjectDevToolsIntegrationFeatureCondition projectDevToolsIntegrationFeatureCondition;

    public BuildsForProjectTabPanel(
            @ComponentImport final FieldVisibilityManager fieldVisibilityManager,
            @ComponentImport final JiraAuthenticationContext jiraAuthenticationContext,
            @ComponentImport final PermissionManager permissionManager,
            final BambooApplicationLinkManager bambooApplicationLinkManager,
            final BambooPanelHelper bambooPanelHelper,
            final ProjectDevToolsIntegrationFeatureCondition projectDevToolsIntegrationFeatureCondition) {
        super(jiraAuthenticationContext);
        this.bambooApplicationLinkManager = checkNotNull(bambooApplicationLinkManager);
        this.bambooPanelHelper = checkNotNull(bambooPanelHelper);
        this.fieldVisibilityManager = checkNotNull(fieldVisibilityManager);
        this.permissionManager = checkNotNull(permissionManager);
        this.projectDevToolsIntegrationFeatureCondition = checkNotNull(projectDevToolsIntegrationFeatureCondition);
    }

    @Override
    public String getHtml(final BrowseContext context) {
        final Project project = context.getProject();

        final Map<String, Object> velocityParams = newHashMap();
        velocityParams.put(MODEL_KEY_FIELD_VISIBILITY, fieldVisibilityManager);
        velocityParams.put(MODEL_KEY_PORTLET, this);
        final String baseLinkUrl =
                "/browse/" + project.getKey() + "?selectedTab=" + BAMBOO_PLUGIN_KEY + ":" + BAMBOO_PLUGIN_MODULE_KEY;
        final String queryString = "projectKey=" + project.getKey();
        bambooPanelHelper.prepareVelocityContext(velocityParams, BAMBOO_PLUGIN_MODULE_KEY, baseLinkUrl, queryString,
                BambooPanelHelper.SUB_TABS, project);
        return descriptor.getHtml(VIEW_RESOURCE, velocityParams);
    }

    @Override
    public boolean showPanel(final BrowseContext context) {
        return bambooApplicationLinkManager.hasApplicationLinks(UNFILTERED)
                && permissionManager.hasPermission(
                VIEW_DEV_TOOLS, context.getProject(), context.getUser())
                && projectDevToolsIntegrationFeatureCondition.shouldDisplay(getBrowseContextMap(context));
    }

    private Map<String, Object> getBrowseContextMap(final BrowseContext context) {
        return ImmutableMap.of(
                CONTEXT_KEY_CONTEXT_TYPE, PROJECT_CONTEXT_TYPE,
                CONTEXT_KEY_PROJECT, context.getProject(),
                CONTEXT_KEY_USER, context.getUser()
        );
    }
}
