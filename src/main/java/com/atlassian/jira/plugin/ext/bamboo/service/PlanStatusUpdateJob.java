package com.atlassian.jira.plugin.ext.bamboo.service;

import com.atlassian.scheduler.JobRunner;
import com.atlassian.scheduler.JobRunnerRequest;
import com.atlassian.scheduler.JobRunnerResponse;
import com.atlassian.scheduler.config.JobRunnerKey;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

import static com.google.common.base.Preconditions.checkNotNull;

public final class PlanStatusUpdateJob implements JobRunner {
    public static final JobRunnerKey KEY = JobRunnerKey.of(PlanStatusUpdateJob.class.getName());

    private static final Logger LOGGER = LoggerFactory.getLogger(PlanStatusUpdateJob.class);

    private final PlanResultStatusUpdateService planResultStatusUpdateService;

    public PlanStatusUpdateJob(final PlanResultStatusUpdateService planResultStatusUpdateService) {
        this.planResultStatusUpdateService = checkNotNull(planResultStatusUpdateService);
    }

    @Nullable
    @Override
    public JobRunnerResponse runJob(@Nonnull final JobRunnerRequest request) {
        final int updatesScheduled = planResultStatusUpdateService.scheduleUpdates();
        if (updatesScheduled > 0) {
            LOGGER.debug("Scheduled {} plan status update(s)", updatesScheduled);
        }
        return JobRunnerResponse.success();
    }
}
