package com.atlassian.jira.plugin.ext.bamboo.rest;

import com.atlassian.applinks.api.ApplicationId;
import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkRequestFactory;
import com.atlassian.applinks.api.CredentialsRequiredException;
import com.atlassian.jira.plugin.ext.bamboo.PluginConstants;
import com.atlassian.jira.plugin.ext.bamboo.applinks.BambooApplicationLinkManager;
import com.atlassian.jira.plugin.ext.bamboo.model.ErrorMessage;
import com.atlassian.jira.plugin.ext.bamboo.model.OAuthErrorMessage;
import com.atlassian.jira.plugin.ext.bamboo.model.RestResult;
import com.atlassian.jira.plugin.ext.bamboo.server.BambooServerInfo;
import com.atlassian.jira.plugin.ext.bamboo.service.BambooRestService;
import com.atlassian.plugin.spring.scanner.annotation.component.Scanned;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.atlassian.sal.api.message.I18nResolver;
import com.google.common.collect.Lists;
import com.google.common.collect.Ordering;
import org.apache.commons.httpclient.HttpStatus;
import org.apache.commons.lang.builder.CompareToBuilder;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.util.Collection;
import java.util.List;
import java.util.Set;

import static com.atlassian.jira.plugin.ext.bamboo.PluginConstants.BAMBOO_ERROR_CONNECTIVITY_I18N_KEY;
import static com.atlassian.jira.plugin.ext.bamboo.applinks.BambooApplicationLinkManager.Filter.SKIP_LINKS_WITH_DEPLOYMENT_SUMMARY_CAPABILITY_IF_FUSION_ENABLED;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Sets.newHashSet;

@Path("/deploy")
@Scanned
public class BambooDeploymentProjectResource {
    public static final int BAMBOO_5_1_BUILD_NUMBER = 3815;

    public static final String BAMBOO_ID_KEY = "bambooId";
    public static final String ID_KEY = "id";

    private static final Logger log = Logger.getLogger(BambooDeploymentProjectResource.class);

    private static final String APPLINK_ID_JSON_KEY = "applinkId";
    private static final String DEP_PROJECT_NAME_JSON_KEY = "name";
    private static final String DEPLOYMENT_PROJECTS_JSON_KEY = "deploymentProjects";
    private static final String OAUTH_CALLBACK_KEY = "oauthCallback";
    private static final String OAUTH_CALLBACKS_KEY = "oauthCallbacks";
    private static final String SERVER_NAME_KEY = "serverName";

    private final BambooApplicationLinkManager bambooApplicationLinkManager;
    private final BambooRestService bambooRestService;
    private final I18nResolver i18nResolver;

    public BambooDeploymentProjectResource(
            @ComponentImport final I18nResolver i18nResolver,
            final BambooRestService bambooRestService,
            final BambooApplicationLinkManager bambooApplicationLinkManager) {
        this.bambooApplicationLinkManager = checkNotNull(bambooApplicationLinkManager);
        this.bambooRestService = checkNotNull(bambooRestService);
        this.i18nResolver = checkNotNull(i18nResolver);
    }

    @GET
    @Path("/{jiraIssueKey}/{applinkId}/{deploymentProjectId}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getIssueDeploymentStatus(@PathParam("deploymentProjectId") String deploymentProjectId,
                                             @PathParam("jiraIssueKey") String jiraIssueKey,
                                             @PathParam(APPLINK_ID_JSON_KEY) String applinkId) {
        ApplicationLink bambooApplicationLink = bambooApplicationLinkManager.getBambooApplicationLink(applinkId);

        if (bambooApplicationLink == null) {
            return Response.serverError().build();
        }

        try {
            RestResult<JSONObject> result = bambooRestService.getIssueDeploymentStatus(bambooApplicationLink, jiraIssueKey, deploymentProjectId);
            JSONObject jsonObject = result.getResult();
            if (jsonObject == null) {
                final ErrorMessage errorMessage = new ErrorMessage(i18nResolver.getText(PluginConstants.BAMBOO_UNREACHABLE_TITLE_I18N_KEY),
                        result.getErrorMessage(i18nResolver.getText(BAMBOO_ERROR_CONNECTIVITY_I18N_KEY, bambooApplicationLink.getName())));
                return errorMessage.createJSONEntity(Response.status(Response.Status.NOT_FOUND)).build();
            }
            return Response.ok(jsonObject.toString(), MediaType.APPLICATION_JSON_TYPE).build();
        } catch (CredentialsRequiredException e) {
            log.debug(PluginConstants.CREDENTIALS_REQUIRED, e);
            return new OAuthErrorMessage(i18nResolver.getText(PluginConstants.LOGIN_AND_APPROVE_BEFORE_DEPLOYMENT_VISIBLE_I18N_KEY, bambooApplicationLink.getName()), e.getAuthorisationURI()).createJSONEntity(Response.status(Response.Status.UNAUTHORIZED)).build();
        } catch (RuntimeException e) {
            log.error("Unexpected server error", e);
            return Response.serverError().build();
        }
    }

    @GET
    @Path("/{jiraProjectKey}/{jiraIssueKey}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response getIssueDeploymentStatus(
            @PathParam("jiraProjectKey") String jiraProjectKey,
            @PathParam("jiraIssueKey") String jiraIssueKey) {
        final Iterable<ApplicationLink> bambooApplicationLinks = bambooApplicationLinkManager.getApplicationLinks(
                SKIP_LINKS_WITH_DEPLOYMENT_SUMMARY_CAPABILITY_IF_FUSION_ENABLED);

        try {
            final JSONObject aggregatedResult = new JSONObject();
            final List<JSONObject> deploymentProjectsJSONObjects = Lists.newArrayList();

            for (final ApplicationLink bambooApplicationLink : bambooApplicationLinks) {
                final Set<String> associatedJiraProjects =
                        newHashSet(bambooApplicationLinkManager.getProjects(bambooApplicationLink.getId().toString()));
                if (associatedJiraProjects.isEmpty() || associatedJiraProjects.contains(jiraProjectKey)) {
                    try {
                        ApplicationLinkRequestFactory requestFactory = bambooApplicationLink.createAuthenticatedRequestFactory();
                        RestResult<JSONObject> result = bambooRestService.getDeploymentProjectsForIssue(
                                requestFactory, bambooApplicationLink.getName(), jiraIssueKey);
                        JSONObject bambooResponse = result.getResult();
                        if (bambooResponse == null) {
                            if (!is404FromOldBamboo(result, requestFactory)) {
                                log.warn(result.getErrorMessage(""));
                            }
                        } else {
                            try {
                                deploymentProjectsJSONObjects.addAll(extractDeploymentProjectsFromResponse(
                                        bambooResponse, bambooApplicationLink.getId()));
                            } catch (JSONException e) {
                                log.warn("", e);
                            }
                        }
                    } catch (CredentialsRequiredException e) {
                        log.debug(PluginConstants.CREDENTIALS_REQUIRED, e);
                        aggregatedResult.append(OAUTH_CALLBACKS_KEY,
                                createOauthCallbackJson(bambooApplicationLink.getName(), e.getAuthorisationURI()));
                    } catch (Exception e) {
                        log.warn("", e);
                    }
                }
            }
            for (JSONObject deploymentJSONObject : DEPLOYMENT_JSON_ORDERING.sortedCopy(deploymentProjectsJSONObjects)) {
                aggregatedResult.append(DEPLOYMENT_PROJECTS_JSON_KEY, deploymentJSONObject);
            }
            return Response.ok(aggregatedResult.toString(), MediaType.APPLICATION_JSON_TYPE).build();
        } catch (Exception e) {
            log.error("Unexpected server error", e);
            return Response.serverError().build();
        }
    }

    private JSONObject createOauthCallbackJson(final String name, final URI authorisationURI) throws JSONException {
        return new JSONObject().put(SERVER_NAME_KEY, name).put(OAUTH_CALLBACK_KEY, authorisationURI.toString());
    }

    private boolean is404FromOldBamboo(RestResult<JSONObject> result, final ApplicationLinkRequestFactory requestFactory) {
        try {
            if (result.getStatusCode() == HttpStatus.SC_NOT_FOUND) {
                RestResult<BambooServerInfo> serverInfoRestResult = bambooRestService.getBambooServerInfo(requestFactory);
                BambooServerInfo serverInfo = serverInfoRestResult.getResult();
                if (serverInfo != null && serverInfo.getBuildNumber() < BAMBOO_5_1_BUILD_NUMBER) {
                    log.info("Bamboo server version " + serverInfo.getVersion() +
                            " does not support Deployments REST endpoint. Suppressing error 404");
                    return true;
                }
            }
        } catch (Exception e) {
            //do nada
        }
        return false;
    }

    private Ordering<JSONObject> DEPLOYMENT_JSON_ORDERING = new Ordering<JSONObject>() {
        @Override
        public int compare(final JSONObject left, final JSONObject right) {
            try {
                if (left == null || right == null) {
                    return (left == right) ? 0 : left == null ? -1 : 1;
                }

                return new CompareToBuilder()
                        .append(left.getString(DEP_PROJECT_NAME_JSON_KEY), right.getString(DEP_PROJECT_NAME_JSON_KEY))
                        .toComparison();
            } catch (JSONException e) {
                throw new RuntimeException(e);
            }
        }
    };

    private Collection<? extends JSONObject> extractDeploymentProjectsFromResponse(
            final JSONObject bambooResponse, final ApplicationId bambooApplinkId) throws JSONException {
        List<JSONObject> result = Lists.newArrayList();
        JSONArray deploymentsArray = bambooResponse.getJSONArray(DEPLOYMENT_PROJECTS_JSON_KEY);
        for (int i = 0; i < deploymentsArray.length(); i++) {
            JSONObject deploymentJSONObject = deploymentsArray.getJSONObject(i);
            Integer bambooId = deploymentJSONObject.getInt(ID_KEY);
            // replace "id" key with "bambooId" to avoid id conflicts in Backbone
            deploymentJSONObject.remove(ID_KEY);
            deploymentJSONObject.put(BAMBOO_ID_KEY, bambooId);
            // making sure that response has name in it at least: better if it barfs here than during sort
            deploymentJSONObject.get(DEP_PROJECT_NAME_JSON_KEY);
            deploymentJSONObject.put(APPLINK_ID_JSON_KEY, bambooApplinkId.toString());
            result.add(deploymentJSONObject);
        }
        return result;
    }
}
