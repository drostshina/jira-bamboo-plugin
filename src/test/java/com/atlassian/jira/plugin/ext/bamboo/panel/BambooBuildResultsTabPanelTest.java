package com.atlassian.jira.plugin.ext.bamboo.panel;

import com.atlassian.jira.issue.Issue;
import com.atlassian.jira.permission.ProjectPermissions;
import com.atlassian.jira.plugin.ext.bamboo.applinks.BambooApplicationLinkManager;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.software.api.conditions.ProjectDevToolsIntegrationFeatureCondition;
import com.atlassian.jira.user.ApplicationUser;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.atlassian.jira.software.api.conditions.ProjectDevToolsIntegrationFeatureCondition.CONTEXT_KEY_PROJECT;
import static org.hamcrest.Matchers.hasEntry;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.argThat;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
@SuppressWarnings("deprecation")
public class BambooBuildResultsTabPanelTest {
    @Mock
    private BambooPanelHelper bambooPanelHelper;

    @Mock
    private BambooApplicationLinkManager bambooApplicationLinkManager;

    @Mock
    private PermissionManager permissionManager;

    @Mock
    private Issue issue;

    @Mock
    private Project project;

    @Mock
    private ProjectDevToolsIntegrationFeatureCondition projectDevToolsIntegrationFeatureCondition;

    private final String PROJECT_KEY = "TST";
    private final String ISSUE_KEY = "TST-1";
    private final ApplicationUser applicationUser = null;

    private BambooBuildResultsTabPanel bambooBuildResultsTabPanel;

    @Before
    public void setUp() throws Exception {
        bambooBuildResultsTabPanel = new TestBambooBuildResultsTabPanel();
        when(issue.getKey()).thenReturn(ISSUE_KEY);
        when(issue.getProjectObject()).thenReturn(project);
        when(project.getKey()).thenReturn(PROJECT_KEY);
        mockProjectDevToolsIntegrationFeatureCondition(true);
        mockAppLink(true);
        mockPermissionManager(true);
    }

    @Test
    public void testGetActionsReturnsBambooBuildResultsAction() {
        List actions = bambooBuildResultsTabPanel.getActions(issue, applicationUser);

        assertEquals(1, actions.size());
        assertEquals(BambooBuildResultsAction.class, actions.get(0).getClass());

        BambooBuildResultsAction bambooBuildResultsAction = (BambooBuildResultsAction) actions.get(0);

        Map<String, Object> actualVelocityContext = new HashMap<String, Object>();
        Map<String, Object> expectedVelocityContext = new HashMap<String, Object>();

        bambooBuildResultsAction.populateVelocityParams(actualVelocityContext);

        assertFalse(bambooBuildResultsAction.isDisplayActionAllTab());

        expectedVelocityContext.put("issueKey", ISSUE_KEY);

        assertEquals(expectedVelocityContext, actualVelocityContext);

        verify(bambooPanelHelper).prepareVelocityContext(
                actualVelocityContext,
                "bamboo-build-results-tabpanel",
                "/browse/" + ISSUE_KEY +
                        "?selected=" + BambooPanelHelper.BAMBOO_PLUGIN_KEY + ":bamboo-build-results-tabpanel",
                "issueKey=" + ISSUE_KEY,
                null,
                issue.getProjectObject()
        );

    }

    @Test
    public void testPanelNotShownIfUserDoesNotHaveViewVersionControlPermission() {
        mockPermissionManager(false);

        assertFalse(bambooBuildResultsTabPanel.showPanel(issue, applicationUser));
    }

    @Test
    public void testPanelNotShownIfBambooNotConfigured() {
        mockAppLink(false);

        assertFalse(bambooBuildResultsTabPanel.showPanel(issue, applicationUser));
    }

    @Test
    public void testPanelNotShownIfProjectDevToolsIntegrationFeatureConditionIsNotSatisfied() {
        mockProjectDevToolsIntegrationFeatureCondition(false);

        assertFalse(bambooBuildResultsTabPanel.showPanel(issue, applicationUser));
    }

    @Test
    public void testPanelShownIfBambooConfiguredAndUserHasRequiredPermission() {
        assertTrue(bambooBuildResultsTabPanel.showPanel(issue, applicationUser));
    }

    private void mockPermissionManager(final boolean hasPermission) {
        when(
                permissionManager.hasPermission(
                        eq(ProjectPermissions.VIEW_DEV_TOOLS),
                        eq(issue),
                        eq(applicationUser)
                )
        ).thenReturn(hasPermission);
    }

    private void mockProjectDevToolsIntegrationFeatureCondition(final boolean satisfied) {
        when(projectDevToolsIntegrationFeatureCondition.shouldDisplay(
                (Map<String, Object>) argThat(hasEntry(CONTEXT_KEY_PROJECT, (Object) project)))).thenReturn(satisfied);
    }

    private void mockAppLink(final boolean hasValidAppLink) {
        when(bambooApplicationLinkManager.hasValidApplicationLinkForIssueBuildPanel(PROJECT_KEY)).thenReturn(hasValidAppLink);
    }

    private class TestBambooBuildResultsTabPanel extends BambooBuildResultsTabPanel {
        private TestBambooBuildResultsTabPanel() {
            super(permissionManager, bambooApplicationLinkManager, bambooPanelHelper, projectDevToolsIntegrationFeatureCondition);
        }
    }
}
