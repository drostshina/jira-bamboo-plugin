package com.atlassian.jira.plugin.ext.bamboo.rest;

import com.atlassian.jira.plugin.ext.bamboo.service.ReleaseErrorReportingService;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.version.Version;
import com.atlassian.jira.project.version.VersionManager;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import javax.ws.rs.core.Response;

import static javax.ws.rs.core.Response.Status.OK;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class JBAMErrorManagementResourceTest {
    private static final long VERSION_ID = 1000L;
    private static final String PROJECT_KEY = "KEY";

    @Mock
    private ReleaseErrorReportingService releaseErrorReportingService;
    @Mock
    private VersionManager versionManager;
    @InjectMocks
    private JBAMErrorManagementResource resource;

    @Mock
    private Version version;
    @Mock
    private Project project;

    @Test
    public void testDeleteErrorsForNullVersionId() throws Exception {
        Response response = resource.deleteErrors(null);
        assertThat(response.getStatus(), is(Response.Status.BAD_REQUEST.getStatusCode()));
    }

    @Test
    public void testDeleteErrorsForInvalidVersionId() throws Exception {
        when(versionManager.getVersion(VERSION_ID)).thenReturn(null);

        Response response = resource.deleteErrors(VERSION_ID);
        assertThat(response.getStatus(), is(Response.Status.NOT_FOUND.getStatusCode()));
    }

    @Test
    public void testDeleteErrorsHappyPath() throws Exception {
        when(versionManager.getVersion(VERSION_ID)).thenReturn(version);
        when(version.getProject()).thenReturn(project);
        when(project.getKey()).thenReturn(PROJECT_KEY);
        when(version.getId()).thenReturn(VERSION_ID);

        Response response = resource.deleteErrors(VERSION_ID);

        assertThat(response.getStatus(), is(OK.getStatusCode()));
        Mockito.verify(releaseErrorReportingService).clearErrors(PROJECT_KEY, VERSION_ID);
    }
}