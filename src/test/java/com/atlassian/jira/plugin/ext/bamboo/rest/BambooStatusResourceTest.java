package com.atlassian.jira.plugin.ext.bamboo.rest;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkRequestFactory;
import com.atlassian.jira.plugin.ext.bamboo.applinks.BambooApplicationLinkManager;
import com.atlassian.jira.plugin.ext.bamboo.model.BuildState;
import com.atlassian.jira.plugin.ext.bamboo.model.LifeCycleState;
import com.atlassian.jira.plugin.ext.bamboo.model.PlanKeys;
import com.atlassian.jira.plugin.ext.bamboo.model.PlanResultKey;
import com.atlassian.jira.plugin.ext.bamboo.model.PlanResultStatus;
import com.atlassian.jira.plugin.ext.bamboo.model.RestResult;
import com.atlassian.jira.plugin.ext.bamboo.service.BambooReleaseService;
import com.atlassian.jira.plugin.ext.bamboo.service.BambooRestService;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.version.Version;
import com.atlassian.jira.project.version.VersionManager;
import com.atlassian.sal.api.message.I18nResolver;
import org.json.JSONObject;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static com.atlassian.jira.plugin.ext.bamboo.PluginConstants.PS_BUILD_COMPLETED_STATE;
import static com.atlassian.jira.plugin.ext.bamboo.model.BuildState.FAILED;
import static com.atlassian.jira.plugin.ext.bamboo.model.BuildState.SUCCESS;
import static com.atlassian.jira.plugin.ext.bamboo.model.LifeCycleState.FINISHED;
import static java.util.Collections.emptyList;
import static org.mockito.Matchers.anyLong;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class BambooStatusResourceTest {
    @Test
    public void testForceRelease() throws Exception {
        assertForceRefreshFlag(true, SUCCESS.name(), SUCCESS, FINISHED, false);
        assertForceRefreshFlag(false, FAILED.name(), FAILED, FINISHED, false);
        assertForceRefreshFlag(false, null, BuildState.UNKNOWN, LifeCycleState.IN_PROGRESS, false);
        assertForceRefreshFlag(false, null, FAILED, FINISHED, true);
        assertForceRefreshFlag(false, null, SUCCESS, FINISHED, true);
        assertForceRefreshFlag(false, SUCCESS.name(), SUCCESS, FINISHED, false);
    }

    private void assertForceRefreshFlag(final boolean versionReleased,
                                        final String storedState,
                                        final BuildState statusState,
                                        final LifeCycleState lifeCycleState,
                                        final boolean shouldRelease) throws Exception {
        // setup
        final Map<String, String> buildData = new HashMap<>();
        buildData.put(PS_BUILD_COMPLETED_STATE, storedState);

        final PlanResultKey planResultKey = PlanKeys.getPlanResultKey("BAM-MAIN-23");
        final long versionId = 1234;
        final PlanResultStatus planResultStatus = new PlanResultStatus(planResultKey, statusState, lifeCycleState, true);
        final String TESTPROJECT = "TESTPROJECT";

        final ApplicationLinkRequestFactory factory = mock(ApplicationLinkRequestFactory.class);
        final ApplicationLink link = mock(ApplicationLink.class);
        when(link.createAuthenticatedRequestFactory()).thenReturn(factory);
        final BambooApplicationLinkManager applicationLinkManager = mock(BambooApplicationLinkManager.class);
        when(applicationLinkManager.getApplicationLink(anyString())).thenReturn(link);

        final Project project = mock(Project.class);
        when(project.getKey()).thenReturn(TESTPROJECT);

        final Version version = mock(Version.class);
        when(version.getProject()).thenReturn(project);
        when(version.isReleased()).thenReturn(versionReleased);

        final VersionManager versionManager = mock(VersionManager.class);
        when(versionManager.getVersion(versionId)).thenReturn(version);

        final BambooRestService restService = mock(BambooRestService.class);
        when(restService.getPlanResultJson(factory, planResultKey))
                .thenReturn(new RestResult<>(new JSONObject(), emptyList(), 0));

        final BambooReleaseService releaseService = mock(BambooReleaseService.class);
        when(releaseService.getBuildData(anyString(), anyLong())).thenReturn(buildData);

        final I18nResolver i18nResolver = mock(I18nResolver.class);

        final BambooStatusResource statusResource = new BambooStatusResource(
                i18nResolver, versionManager, applicationLinkManager, releaseService, restService) {
            @Override
            protected PlanResultStatus getPlanResultStatusFromJSON(
                    final PlanResultKey planResultKey, final JSONObject bambooJsonObject) {
                return planResultStatus;
            }
        };

        // perform
        statusResource.getStatus(planResultKey.getKey(), versionId);

        // verify
        if (shouldRelease) {
            verify(releaseService, times(1)).releaseIfRequired(planResultStatus, version);
        } else {
            verify(releaseService, never()).releaseIfRequired(planResultStatus, version);
        }
    }
}
