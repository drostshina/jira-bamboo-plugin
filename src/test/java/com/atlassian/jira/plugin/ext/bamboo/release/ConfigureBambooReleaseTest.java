package com.atlassian.jira.plugin.ext.bamboo.release;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.AuthorisationURIGenerator;
import com.atlassian.applinks.api.CredentialsRequiredException;
import com.atlassian.fugue.Either;
import com.atlassian.jira.bc.project.version.VersionService;
import com.atlassian.jira.datetime.DateTimeFormatter;
import com.atlassian.jira.datetime.DateTimeFormatterFactory;
import com.atlassian.jira.datetime.DateTimeStyle;
import com.atlassian.jira.junit.rules.AvailableInContainer;
import com.atlassian.jira.junit.rules.MockComponentContainer;
import com.atlassian.jira.plugin.ext.bamboo.applinks.BambooApplicationLinkManager;
import com.atlassian.jira.plugin.ext.bamboo.model.PlanKeys;
import com.atlassian.jira.plugin.ext.bamboo.model.PlanResultKey;
import com.atlassian.jira.plugin.ext.bamboo.service.BambooReleaseService;
import com.atlassian.jira.plugin.ext.bamboo.service.BambooRestService;
import com.atlassian.jira.plugin.ext.bamboo.service.PlanExecutionResult;
import com.atlassian.jira.plugin.ext.bamboo.service.ProjectVersionService;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.ProjectManager;
import com.atlassian.jira.project.version.Version;
import com.atlassian.jira.project.version.VersionManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.user.ApplicationUser;
import com.atlassian.jira.util.DateFieldFormat;
import com.atlassian.jira.util.I18nHelper;
import com.google.common.collect.ImmutableMap;
import org.joda.time.DateTime;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import java.net.URI;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.Map;
import java.util.TimeZone;

import static com.atlassian.jira.plugin.ext.bamboo.PluginConstants.ADMIN_ERROR_VERSION_NO_PERMISSION_I18N_KEY;
import static com.atlassian.jira.plugin.ext.bamboo.PluginConstants.BAMBOO_ERROR_CONNECTIVITY_CONTINUE_RELEASE_I18N_KEY;
import static com.atlassian.jira.plugin.ext.bamboo.PluginConstants.BUILD_TYPE_EXISTING_BUILD;
import static com.atlassian.jira.plugin.ext.bamboo.PluginConstants.ERROR_CONFIGURATION_I18N_KEY;
import static com.atlassian.jira.plugin.ext.bamboo.PluginConstants.ERROR_INVALID_PLAN_I18N_KEY;
import static com.atlassian.jira.plugin.ext.bamboo.PluginConstants.ERROR_INVALID_VERSION_I18N_KEY;
import static com.atlassian.jira.plugin.ext.bamboo.PluginConstants.ERROR_USER_NOT_AUTHORISED_I18N_KEY;
import static com.atlassian.jira.plugin.ext.bamboo.PluginConstants.ERROR_VERSION_WITHOUT_PROJECT_I18N_KEY;
import static com.atlassian.jira.plugin.ext.bamboo.PluginConstants.JIRA_ERROR_NOT_RECOVERABLE_I18N_KEY;
import static com.atlassian.jira.plugin.ext.bamboo.PluginConstants.PS_CONFIG_RELEASE_DATE;
import static com.atlassian.jira.plugin.ext.bamboo.PluginConstants.VARIABLE_PARAM_PREFIX;
import static com.atlassian.jira.plugin.ext.bamboo.release.ConfigureBambooRelease.INCORRECT_DATE_FORMAT;
import static com.atlassian.jira.plugin.ext.bamboo.release.ConfigureBambooRelease.RELEASE_DATE_FIELD;
import static java.util.Collections.emptyList;
import static java.util.Collections.singletonMap;
import static org.hamcrest.Matchers.hasEntry;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ConfigureBambooReleaseTest {
    private static final String BAMBOO_CONNECTIVITY_ERROR = "BAMBOO_CONNECTIVITY_ERROR";
    private static final String JIRA_ERROR_NOT_RECOVERABLE = "JIRA_ERROR_NOT_RECOVERABLE";
    public static final String ERROR_USER_NOT_AUTHORISED = "ERROR_USER_NOT_AUTHORISED";

    private static final String ERROR_INVALID_VERSION = "ERROR_INVALID_VERSION";
    private static final String ERROR_INVALID_PLAN = "ERROR_USER_NOT_AUTHORISED";
    private static final String ERROR_VERSION_WITHOUT_PROJECT = "ERROR_VERSION_WITHOUT_PROJECT";
    private static final String ERROR_CONFIGURATION = "ERROR_CONFIGURATION";
    private static final String ADMIN_ERROR_VERSION_NO_PERMISSION = "ADMIN_ERROR_VERSION_NO_PERMISSION";

    private static final String SERVER_NAME = "Bamboo instance";
    private static final String PLAN_RESULT_KEY = "TEST-BBB-10";

    private static final long PROJECT_ID = 789;
    private static final long VERSION_ID = 23456;

    private static final Collection<String> NO_ERRORS = emptyList();
    private static final String PROJECT_KEY = "TWIT";
    private static final Map<String, String> SETTINGS = Collections.emptyMap();


    @Rule
    public MockComponentContainer container = new MockComponentContainer(this);

    // Constructor dependencies
    @Mock
    private BambooApplicationLinkManager bambooApplicationLinkManager;
    @Mock
    private BambooReleaseService bambooReleaseService;
    @Mock
    private BambooRestService bambooRestService;
    @Mock
    private DateFieldFormat dateFieldFormat;
    @Mock
    private PermissionManager permissionManager;
    @Mock
    private ProjectManager projectManager;
    @Mock
    private ProjectVersionService projectVersionService;
    @Mock
    private VersionManager versionManager;
    @Mock
    private VersionService versionService;

    // ComponentAccessor dependencies
    @AvailableInContainer
    @Mock
    private JiraAuthenticationContext jiraAuthenticationContext;
    @AvailableInContainer
    @Mock
    private DateTimeFormatterFactory dateTimeFormatterFactory;

    // Other mocks
    @Mock
    private ApplicationUser applicationUser;
    @Mock
    private I18nHelper i18nHelper;
    @Mock
    private Project project;
    @Mock
    private Version version;
    @Mock
    private ApplicationLink applicationLink;
    @Mock
    private AuthorisationURIGenerator authorisationURIGenerator;
    @Mock
    private DateTimeFormatter dateTimeFormatter;

    // this is used to test that the settings map created by
    // action.doExecute() contains the things we expect
    @Captor
    ArgumentCaptor<Map<String, String>> settings;

    @InjectMocks
    @Spy
    private ConfigureBambooRelease action;

    @Before
    public void setUp() {
        action.setVersionId(VERSION_ID);

        when(jiraAuthenticationContext.getI18nHelper()).thenReturn(i18nHelper);
        setUpUser();
        when(versionManager.getVersion(VERSION_ID)).thenReturn(version);

        when(project.getId()).thenReturn(PROJECT_ID);
        when(project.getKey()).thenReturn(PROJECT_KEY);
        when(version.getProject()).thenReturn(project);
        when(version.getProjectId()).thenReturn(PROJECT_ID);
        when(applicationLink.getName()).thenReturn(SERVER_NAME);

        when(action.getI18nText(BAMBOO_ERROR_CONNECTIVITY_CONTINUE_RELEASE_I18N_KEY, SERVER_NAME)).thenReturn(BAMBOO_CONNECTIVITY_ERROR);
        when(action.getI18nText(JIRA_ERROR_NOT_RECOVERABLE_I18N_KEY, SERVER_NAME)).thenReturn(JIRA_ERROR_NOT_RECOVERABLE);

        when(action.getI18nText(eq(ERROR_VERSION_WITHOUT_PROJECT_I18N_KEY), anyString())).thenReturn(ERROR_VERSION_WITHOUT_PROJECT);
        when(action.getI18nText(ERROR_CONFIGURATION_I18N_KEY)).thenReturn(ERROR_CONFIGURATION);
        when(action.getI18nText(ADMIN_ERROR_VERSION_NO_PERMISSION_I18N_KEY)).thenReturn(ADMIN_ERROR_VERSION_NO_PERMISSION);
        when(action.getI18nText(ERROR_INVALID_PLAN_I18N_KEY)).thenReturn(ERROR_INVALID_PLAN);
        when(action.getI18nText(ERROR_INVALID_VERSION_I18N_KEY, String.valueOf(VERSION_ID))).thenReturn(ERROR_INVALID_VERSION);

        setupDateTimeFormatter();
    }

    @SuppressWarnings("deprecation")
    private void setUpUser() {
        // The production code under test calls getUser via its base class
        when(jiraAuthenticationContext.getUser()).thenReturn(applicationUser);
    }

    private void setupDateTimeFormatter() {
        when(dateTimeFormatterFactory.formatter()).thenReturn(dateTimeFormatter);
        when(dateTimeFormatter.forLoggedInUser()).thenReturn(dateTimeFormatter);
        when(dateTimeFormatter.withStyle(any(DateTimeStyle.class))).thenReturn(dateTimeFormatter);
        when(dateTimeFormatter.withZone(any(TimeZone.class))).thenReturn(dateTimeFormatter);
        when(projectVersionService.getUnresolvedIssueCount(PROJECT_ID, VERSION_ID))
                .thenReturn(Either.<String, Integer>right(0));
    }

    @Test
    public void validationShouldHandleNonExistentVersion() {
        // Set up
        setUpVersion(null);

        // Invoke
        action.doValidation();

        // Check
        assertThat(action.getErrorMessages(), hasItem(ERROR_INVALID_VERSION));
    }

    private void setUpVersion(final Version version) {
        when(versionManager.getVersion(VERSION_ID)).thenReturn(version);
    }

    @Test
    public void validationShouldAcceptEmptyReleaseDate() {
        assertValidReleaseDate("", NO_ERRORS);
        verifyNoMoreInteractions(dateFieldFormat, versionService);
    }

    @Test
    public void validationShouldAcceptNullReleaseDate() {
        assertValidReleaseDate(null, NO_ERRORS);
        verifyNoMoreInteractions(dateFieldFormat, versionService);
    }

    private void assertValidReleaseDate(final String userReleaseDate, final Collection<String> expectedErrorMessages) {
        // Set up
        action.setBuildType(BUILD_TYPE_EXISTING_BUILD);
        action.setUserReleaseDate(userReleaseDate);
        setUpVersion(version);
        setUpUnresolvedIssueCount(0);
        setUpProjectPermissions(true);

        // Invoke
        action.doValidation();

        // Check
        assertThat(action.getErrorMessages(), is(expectedErrorMessages));
    }

    @Test
    public void validationShouldRejectInvalidReleaseDate() {
        // Set up
        final String userReleaseDate = "something invalid";
        final String formatHint = "like this";
        final String errorMessage = "Fix it!";
        action.setBuildType(BUILD_TYPE_EXISTING_BUILD);
        action.setUserReleaseDate(userReleaseDate);
        setUpVersion(version);
        when(dateFieldFormat.parseDatePicker(userReleaseDate)).thenThrow(new IllegalArgumentException());
        when(dateFieldFormat.getFormatHint()).thenReturn(formatHint);
        when(i18nHelper.getText(INCORRECT_DATE_FORMAT, formatHint)).thenReturn(errorMessage);
        when(bambooApplicationLinkManager.getApplicationLink(PROJECT_KEY)).thenReturn(null);
        when(versionManager.getVersionsUnreleased(PROJECT_ID, true)).thenReturn(Collections.<Version>emptyList());
        setUpUnresolvedIssueCount(0);

        // Invoke
        action.doValidation();

        // Check
        final Map<String, String> expectedErrorMap = singletonMap(RELEASE_DATE_FIELD, errorMessage);
        assertThat(action.getErrors(), is(expectedErrorMap));
    }

    @Test
    public void shouldReturnConnectivityErrorWhenTriggeringNewBuildFails() throws Exception {
        PlanExecutionResult result = new PlanExecutionResult(PlanKeys.getPlanResultKey(PLAN_RESULT_KEY), "Bamboo failure");
        action.setBuildResult(PLAN_RESULT_KEY);

        when(bambooReleaseService.triggerPlanForRelease(eq(applicationLink), any(Version.class), eq(SETTINGS))).thenReturn(result);

        final String error = action.triggerReleaseBuild(true, applicationLink, SETTINGS);
        assertThat(error, is(BAMBOO_CONNECTIVITY_ERROR));
    }

    @Test
    public void shouldReturnConnectivityErrorWhenTriggeringExistingBuildFails() throws Exception {
        PlanExecutionResult result = new PlanExecutionResult(PlanKeys.getPlanResultKey(PLAN_RESULT_KEY), "Bamboo failure");
        action.setBuildResult(PLAN_RESULT_KEY);

        when(bambooReleaseService.triggerExistingBuildForRelease(eq(applicationLink), any(Version.class), any(PlanResultKey.class), eq(SETTINGS))).thenReturn(result);

        final String error = action.triggerReleaseBuild(false, applicationLink, SETTINGS);
        assertThat(error, is(BAMBOO_CONNECTIVITY_ERROR));
    }

    @Test
    public void shouldReturnSystemErrorWhenTriggeringNewBuildFailsUnexpectedly() throws Exception {
        when(bambooReleaseService.triggerPlanForRelease(eq(applicationLink), any(Version.class), eq(SETTINGS))).thenThrow(new IllegalStateException("database error"));
        final String error = action.triggerReleaseBuild(true, applicationLink, SETTINGS);
        assertThat(error, is(JIRA_ERROR_NOT_RECOVERABLE));
    }

    @Test
    public void shouldReturnAuthorisationErrorWhenTriggeringNewBuildFails() throws Exception {
        final URI authURI = new URI("http://server.com/bla");
        when(authorisationURIGenerator.getAuthorisationURI()).thenReturn(authURI);
        CredentialsRequiredException authError = new CredentialsRequiredException(authorisationURIGenerator, "User not authorised");

        when(bambooReleaseService.triggerPlanForRelease(eq(applicationLink), any(Version.class), eq(SETTINGS))).thenThrow(authError);
        when(action.getI18nText(ERROR_USER_NOT_AUTHORISED_I18N_KEY, authURI.toString())).thenReturn(ERROR_USER_NOT_AUTHORISED);

        final String error = action.triggerReleaseBuild(true, applicationLink, SETTINGS);
        assertThat(error, is(ERROR_USER_NOT_AUTHORISED));
    }

    private void setUpProjectPermissions(final Boolean hasPermission) {
        when(bambooReleaseService.hasPermissionToRelease(applicationUser, project)).thenReturn(hasPermission);
    }

    private void setUpUnresolvedIssueCount(final int issueCount) {
        when(projectVersionService.getUnresolvedIssueCount(PROJECT_ID, VERSION_ID))
                .thenReturn(Either.<String, Integer>right(issueCount));
    }

    @Test
    public void theReleaseDateStringSavedInTheSettingsMapShouldBeParsableUsingParseDatePicker() {
        // setup
        String expectedValue = "we used formatDatePicker!";
        when(dateFieldFormat.formatDatePicker(any(Date.class))).thenReturn(expectedValue);

        // execute
        action.doExecute();

        // check
        verify(bambooReleaseService).releaseWithNoBuild(any(Version.class), settings.capture());
        assertThat("The release date string is formatted correctly",
                settings.getValue().get(PS_CONFIG_RELEASE_DATE),
                is(expectedValue));
    }

    @Test
    public void formattedReleaseDateShouldUseDatePickerFormattingAndDefaultTimeZone() {
        // setup
        when(version.getReleaseDate()).thenReturn(DateTime.now().toDate());

        // execute
        action.doInput();

        // check
        verifyDateTimeFormatterInteractions(dateTimeFormatter);
    }

    private void verifyDateTimeFormatterInteractions(final DateTimeFormatter dateTimeFormatter) {
        InOrder dtfInOrder = inOrder(dateTimeFormatter);

        // interactions when we get the dateTimeFormatter from JiraWebActionSupport
        dtfInOrder.verify(dateTimeFormatter).forLoggedInUser();
        dtfInOrder.verify(dateTimeFormatter).withStyle(DateTimeStyle.RELATIVE);

        // interactions in ConfigureBambooRelease to ensure the correct TZ is used and that the formatter works
        // correctly with the calendar picker
        dtfInOrder.verify(dateTimeFormatter).withStyle(DateTimeStyle.DATE_PICKER);
        dtfInOrder.verify(dateTimeFormatter).withZone(TimeZone.getDefault());
        dtfInOrder.verify(dateTimeFormatter).format(version.getReleaseDate());
    }

    @Test
    public void shouldFilterVariableParameters() {
        // Set up
        final String goodKey = "good-key";
        final String goodFirstValue = "non-blank-value";
        final Map<String, String[]> unfilteredParams = ImmutableMap.of(
                "this-key-should-be-ignored", new String[]{"this-value-should-be-ignored"},
                VARIABLE_PARAM_PREFIX + goodKey, new String[]{goodFirstValue, "ignored-second-value"},
                VARIABLE_PARAM_PREFIX + "good-key-with-empty-array", new String[0],
                VARIABLE_PARAM_PREFIX + "good-key-with-empty-first-value", new String[]{"", "xxx"}
        );

        // Invoke
        final Map<String, String> filteredParams = ConfigureBambooRelease.filterVariableParams(unfilteredParams);

        // Check
        assertThat(filteredParams.size(), is(1));
        assertThat(filteredParams, hasEntry(VARIABLE_PARAM_PREFIX + goodKey, goodFirstValue));
    }
}
