package com.atlassian.jira.plugin.ext.bamboo.service;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.applinks.api.ApplicationLinkRequestFactory;
import com.atlassian.applinks.api.CredentialsRequiredException;
import com.atlassian.jira.plugin.ext.bamboo.model.PlanKeys;
import com.atlassian.jira.plugin.ext.bamboo.model.PlanResultKey;
import com.atlassian.jira.plugin.ext.bamboo.model.RestResult;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.util.I18nHelper;
import org.junit.Before;
import org.junit.Test;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.atlassian.jira.plugin.ext.bamboo.PluginConstants.BAMBOO_ERROR_CONNECTIVITY_I18N_KEY;
import static com.atlassian.jira.plugin.ext.bamboo.PluginConstants.BAMBOO_ERROR_RELEASE_RETRY_MANUALLY_I18N_KEY;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.core.Is.is;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class BambooRestServiceImplTest {
    private static final String BAMBOO_ERROR_CONNECTIVITY = "Bamboo connectivity errors";
    private static final String BAMBOO_ERROR_RETRY_RELEASE = "Bamboo release errors, retry releasing manually.";

    private I18nHelper i18nHelper = mock(I18nHelper.class);
    private JiraAuthenticationContext authenticationContext = mock(JiraAuthenticationContext.class);

    @Before
    public void setup() {
        when(authenticationContext.getI18nHelper()).thenReturn(i18nHelper);
        when(i18nHelper.getText(eq(BAMBOO_ERROR_CONNECTIVITY_I18N_KEY), anyString())).thenReturn(BAMBOO_ERROR_CONNECTIVITY);
        when(i18nHelper.getText(eq(BAMBOO_ERROR_RELEASE_RETRY_MANUALLY_I18N_KEY), anyString())).thenReturn(BAMBOO_ERROR_RETRY_RELEASE);
    }

    @Test
    public void testResponseBodyCapturedOnError() throws Exception {
        final String responseBody = "Custom Response Body";

        BambooRestService service = new BambooRestServiceImpl(authenticationContext) {
            @Nonnull
            @Override
            public BambooRestResponse executePostRequest(@Nonnull ApplicationLinkRequestFactory authenticatedRequestFactory, @Nonnull String url, @Nonnull Map<String, String> params, int timeout, String serverName) throws CredentialsRequiredException {
                return new BambooRestResponse(400, "BAD", responseBody, new ArrayList<String>());
            }
        };

        ApplicationLinkRequestFactory requestFactory = mock(ApplicationLinkRequestFactory.class);
        ApplicationLink link = mock(ApplicationLink.class);
        when(link.createAuthenticatedRequestFactory()).thenReturn(requestFactory);
        RestResult<PlanResultKey> restResult = service.triggerPlan(link, PlanKeys.getPlanKey("BAM-MAIN"), null, new HashMap<String, String>());
        List<String> errors = restResult.getErrors();
        assertThat(restResult.getResult(), is(nullValue(PlanResultKey.class)));
        assertThat(errors.size(), is(1));
        assertThat(errors, contains(BAMBOO_ERROR_CONNECTIVITY));
    }

    @Test
    public void testStatusMessageReturnedIfNoBody() throws Exception {
        BambooRestService service = new BambooRestServiceImpl(authenticationContext) {
            @Nonnull
            @Override
            public BambooRestResponse executePostRequest(@Nonnull ApplicationLinkRequestFactory authenticatedRequestFactory, @Nonnull String url, @Nonnull Map<String, String> params, int timeout, String serverName) throws CredentialsRequiredException {
                return new BambooRestResponse(400, "BAD ERROR", "", new ArrayList<String>());
            }
        };

        ApplicationLinkRequestFactory requestFactory = mock(ApplicationLinkRequestFactory.class);
        ApplicationLink link = mock(ApplicationLink.class);
        when(link.createAuthenticatedRequestFactory()).thenReturn(requestFactory);
        RestResult<PlanResultKey> restResult = service.triggerPlan(link, PlanKeys.getPlanKey("BAM-MAIN"), null, new HashMap<String, String>());
        List<String> errors = restResult.getErrors();
        assertThat(restResult.getResult(), is(nullValue(PlanResultKey.class)));
        assertThat(errors.size(), is(1));
        assertThat(errors, contains(BAMBOO_ERROR_CONNECTIVITY));
    }

    @Test
    public void testCredentialsRequiredMessagePropagated() {
        final ApplicationLinkRequestFactory requestFactory = mock(ApplicationLinkRequestFactory.class);
        final ApplicationLink link = mock(ApplicationLink.class);
        when(link.createAuthenticatedRequestFactory()).thenReturn(requestFactory);

        BambooRestService service = new BambooRestServiceImpl(authenticationContext) {
            @Nonnull
            @Override
            public BambooRestResponse executePostRequest(@Nonnull ApplicationLinkRequestFactory authenticatedRequestFactory, @Nonnull String url, @Nonnull Map<String, String> params, int timeout, String serverName) throws CredentialsRequiredException {
                throw new CredentialsRequiredException(requestFactory, "NEED TO LOG IN");
            }
        };

        try {
            service.triggerPlan(link, PlanKeys.getPlanKey("BAM-MAIN"), null, new HashMap<String, String>());
            fail("Credentials Required Exception Should have been thrown");
        } catch (CredentialsRequiredException e) {
            // this is good
        }
    }
}
