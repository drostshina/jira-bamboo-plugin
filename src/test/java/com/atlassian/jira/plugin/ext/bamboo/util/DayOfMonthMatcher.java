package com.atlassian.jira.plugin.ext.bamboo.util;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

import java.util.Date;

import static java.time.temporal.ChronoField.DAY_OF_MONTH;

/**
 * A Hamcrest matcher that checks the day of the month on which a Date falls.
 */
public class DayOfMonthMatcher extends TypeSafeMatcher<Date> {

    public static Matcher<Date> isSameDayOfMonth(final Date expectedDate) {
        return new DayOfMonthMatcher(expectedDate);
    }

    private final int expectedDayOfMonth;

    private DayOfMonthMatcher(final Date expectedDate) {
        this.expectedDayOfMonth = expectedDate.toInstant().get(DAY_OF_MONTH);
    }

    @Override
    protected boolean matchesSafely(final Date date) {
        return date.toInstant().get(DAY_OF_MONTH) == expectedDayOfMonth;
    }

    @Override
    public void describeTo(final Description description) {
        description.appendText("is (\"" + expectedDayOfMonth + "\")");
    }
}
