package com.atlassian.jira.plugin.ext.bamboo.panel;

import com.atlassian.applinks.api.ApplicationLink;
import com.atlassian.jira.mock.component.MockComponentWorker;
import com.atlassian.jira.permission.GlobalPermissionKey;
import com.atlassian.jira.plugin.ext.bamboo.applinks.BambooApplicationLinkManager;
import com.atlassian.jira.plugin.ext.bamboo.service.BambooReleaseService;
import com.atlassian.jira.plugin.ext.bamboo.service.ReleaseErrorReportingService;
import com.atlassian.jira.project.Project;
import com.atlassian.jira.project.version.Version;
import com.atlassian.jira.security.GlobalPermissionManager;
import com.atlassian.jira.security.JiraAuthenticationContext;
import com.atlassian.jira.security.PermissionManager;
import com.atlassian.jira.user.ApplicationUser;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.ImmutableMap;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import webwork.action.ActionContext;

import java.net.URI;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.atlassian.jira.permission.ProjectPermissions.VIEW_DEV_TOOLS;
import static com.atlassian.jira.plugin.ext.bamboo.PluginConstants.PS_BUILD_COMPLETED_STATE;
import static com.atlassian.jira.plugin.ext.bamboo.PluginConstants.PS_BUILD_RESULT;
import static com.atlassian.jira.plugin.ext.bamboo.applinks.BambooApplicationLinkManager.Filter.UNFILTERED;
import static com.atlassian.jira.plugin.ext.bamboo.panel.BambooPanelHelper.BAMBOO_PLUGIN_KEY;
import static com.atlassian.jira.plugin.ext.bamboo.panel.BambooPanelHelper.BUILD_RESULT_KEY;
import static com.atlassian.jira.plugin.ext.bamboo.panel.BambooPanelHelper.BUILD_TRIGGERED_KEY;
import static com.atlassian.jira.plugin.ext.bamboo.panel.BambooPanelHelper.COMPLETED_STATE_KEY;
import static com.atlassian.jira.plugin.ext.bamboo.panel.BambooPanelHelper.DAYS_REMAINING_KEY;
import static com.atlassian.jira.plugin.ext.bamboo.panel.BambooPanelHelper.HAS_APPLINKS_KEY;
import static com.atlassian.jira.plugin.ext.bamboo.panel.BambooPanelHelper.IS_PROJECT_ADMIN_KEY;
import static com.atlassian.jira.plugin.ext.bamboo.panel.BambooPanelHelper.OVERDUE_KEY;
import static com.atlassian.jira.plugin.ext.bamboo.panel.BambooPanelHelper.RELEASE_ERRORS_KEY;
import static com.atlassian.jira.plugin.ext.bamboo.panel.BambooPanelHelper.SELECTED_SUB_TAB_KEY;
import static com.atlassian.jira.plugin.ext.bamboo.panel.BambooPanelHelper.SUB_TAB_BUILD_BY_DATE;
import static com.atlassian.jira.plugin.ext.bamboo.panel.BambooPanelHelper.SUB_TAB_BUILD_BY_PLAN;
import static com.atlassian.jira.plugin.ext.bamboo.panel.BambooPanelHelper.SUB_TAB_PLAN_STATUS;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class BambooPanelHelperTestCase {
    protected static final long VERSION_ID = 156789;
    protected static final String PROJECT_KEY = "TST";
    private static final boolean IS_OVERDUE = false;
    private static final Object DAYS_REMAINING = "null";

    @Mock
    private BambooReleaseService bambooReleaseService;
    @Mock
    private ReleaseErrorReportingService releaseErrorReportingService;
    @Mock
    private ApplicationLink applicationLink;
    @Mock
    private BambooApplicationLinkManager bambooApplicationLinkManager;
    @Mock
    private JiraAuthenticationContext jiraAuthenticationContext;
    @Mock
    private GlobalPermissionManager globalPermissionManager;
    @Mock
    private PermissionManager permissionManager;
    @Mock
    private Project project;

    @InjectMocks
    private BambooPanelHelper bambooPanelHelper;


    @Mock
    protected ApplicationUser applicationUser;
    @Mock
    protected Version version;

    private List<String> subTabs;
    private Map<String, Object> session;
    private Map<String, Object> velocityContext;
    private Map<String, String[]> params;
    private String bambooPluginModuleKey;
    private String baseLinkUrl;
    private String queryString;

    @Before
    public void setUp() throws Exception {
        when(project.getKey()).thenReturn(PROJECT_KEY);
        when(version.getId()).thenReturn(VERSION_ID);
        when(version.isReleased()).thenReturn(true);
        when(applicationUser.getName()).thenReturn("username");

        when(applicationLink.getDisplayUrl()).thenReturn(URI.create("http://localhost:8080/bamboo"));
        when(applicationLink.getRpcUrl()).thenReturn(URI.create("http://localhost:8080/bamboo"));
        when(bambooApplicationLinkManager.getApplicationLink(any(String.class))).thenReturn(applicationLink);

        bambooPluginModuleKey = "xyz";
        queryString = "issueKey=TST-1";
        baseLinkUrl = "/browse/TST-1?selected=" + BAMBOO_PLUGIN_KEY + ":" + bambooPluginModuleKey;
        subTabs = new ArrayList<String>();
        velocityContext = new HashMap<String, Object>();
        params = new HashMap<String, String[]>();
        session = new HashMap<String, Object>();

        ActionContext.setParameters(params);
        ActionContext.setSession(session);

        new MockComponentWorker().init();
    }

    @After
    public void tearDown() throws Exception {
        ActionContext.setParameters(null);
        ActionContext.setSession(null);
    }

    private void setUpCommonEntriesInExpectedVelocityContext(Map<String, Object> expectedVelocityContext) {
        expectedVelocityContext.put("moduleKey", bambooPluginModuleKey);
        expectedVelocityContext.put("querySection", queryString);
        expectedVelocityContext.put("baseLinkUrl", baseLinkUrl);
        expectedVelocityContext.put("baseResourceUrl",
                "/download/resources/" + BAMBOO_PLUGIN_KEY + ":" + bambooPluginModuleKey);
        expectedVelocityContext.put("baseBambooUrl", "http://localhost:8080/bamboo");
        expectedVelocityContext.put("baseRestUrl", "/rest/bamboo/1.0/");
        expectedVelocityContext.put("baseBambooRestProxyUrl", "/rest/bamboo/1.0/proxy/");
        expectedVelocityContext.put("baseBambooRestUrl", "http://localhost:8080/bamboo/rest/api/latest/");
        expectedVelocityContext.put("bambooServerName", null);
    }

    @Test
    public void testSelectedTabDefaultsToBuildByDateTabIfSubTabsNotSpecified() {
        final Map<String, Object> expectedVelocityContext = new HashMap<String, Object>();

        bambooPanelHelper.prepareVelocityContext(
                velocityContext,
                bambooPluginModuleKey,
                baseLinkUrl,
                queryString,
                subTabs,
                project
        );

        expectedVelocityContext.put(SELECTED_SUB_TAB_KEY, SUB_TAB_BUILD_BY_DATE);
        expectedVelocityContext.put("showRss", true);
        expectedVelocityContext.put("isSystemAdmin", false);
        setUpCommonEntriesInExpectedVelocityContext(expectedVelocityContext);

        assertEquals(expectedVelocityContext, velocityContext);
    }

    @Test
    public void testSelectedTabBasedOnSession() {
        final Map<String, Object> expectedVelocityContext = new HashMap<String, Object>();

        session.put(BAMBOO_PLUGIN_KEY + "." + SELECTED_SUB_TAB_KEY, SUB_TAB_PLAN_STATUS);
        subTabs.add("fake");

        bambooPanelHelper.prepareVelocityContext(
                velocityContext,
                bambooPluginModuleKey,
                baseLinkUrl,
                queryString,
                subTabs,
                project
        );

        expectedVelocityContext.put(SELECTED_SUB_TAB_KEY, SUB_TAB_PLAN_STATUS);
        expectedVelocityContext.put("availableTabs", subTabs);
        expectedVelocityContext.put("isSystemAdmin", false);
        setUpCommonEntriesInExpectedVelocityContext(expectedVelocityContext);

        assertEquals(expectedVelocityContext, velocityContext);
    }

    @Test
    public void testSelectedTabInRequestParameterOverridesTheOneInSession() {
        final Map<String, Object> expectedVelocityContext = new HashMap<String, Object>();

        session.put(BAMBOO_PLUGIN_KEY + "." + SELECTED_SUB_TAB_KEY, SUB_TAB_PLAN_STATUS);
        params.put(SELECTED_SUB_TAB_KEY, new String[]{SUB_TAB_BUILD_BY_PLAN});
        subTabs.add("fake");

        bambooPanelHelper.prepareVelocityContext(
                velocityContext,
                bambooPluginModuleKey,
                baseLinkUrl,
                queryString,
                subTabs,
                project
        );

        expectedVelocityContext.put(SELECTED_SUB_TAB_KEY, SUB_TAB_BUILD_BY_PLAN);
        expectedVelocityContext.put("availableTabs", subTabs);
        expectedVelocityContext.put("isSystemAdmin", false);
        setUpCommonEntriesInExpectedVelocityContext(expectedVelocityContext);

        assertEquals(expectedVelocityContext, velocityContext);
        assertEquals(SUB_TAB_BUILD_BY_PLAN,
                session.get(BAMBOO_PLUGIN_KEY + "." + SELECTED_SUB_TAB_KEY));
    }

    @Test
    public void panelShouldAddCorrectVelocityParamsWhenNoBuildDataIsAvailable() {
        // Set up
        final Map<String, Object> expectedVelocityParams = new HashMap<String, Object>();
        expectedVelocityParams.put(HAS_APPLINKS_KEY, true);
        expectedVelocityParams.put(IS_PROJECT_ADMIN_KEY, true);
        expectedVelocityParams.put(DAYS_REMAINING_KEY, DAYS_REMAINING);
        expectedVelocityParams.put(OVERDUE_KEY, IS_OVERDUE);

        // Invoke and check
        assertVelocityParams(true, true, null, Collections.<String>emptyList(), expectedVelocityParams);
    }

    @Test
    public void panelShouldAddCorrectVelocityParamsWhenReleaseHasErrors() {
        // Set up
        final List<String> releaseErrors = ImmutableList.of("error1", "error2");
        final Map<String, Object> expectedVelocityParams = new HashMap<String, Object>();
        expectedVelocityParams.put(HAS_APPLINKS_KEY, true);
        expectedVelocityParams.put(IS_PROJECT_ADMIN_KEY, true);
        expectedVelocityParams.put(RELEASE_ERRORS_KEY, releaseErrors);
        expectedVelocityParams.put(DAYS_REMAINING_KEY, DAYS_REMAINING);
        expectedVelocityParams.put(OVERDUE_KEY, IS_OVERDUE);

        // Invoke and check
        assertVelocityParams(true, true, null, releaseErrors, expectedVelocityParams);
    }

    @Test
    public void panelShouldAddCorrectVelocityParamsWhenEmptyBuildDataIsAvailable() {
        // Set up
        final Map<String, Object> expectedVelocityParams = new HashMap<String, Object>();
        expectedVelocityParams.put(BUILD_TRIGGERED_KEY, true);
        expectedVelocityParams.put(HAS_APPLINKS_KEY, false);
        expectedVelocityParams.put(IS_PROJECT_ADMIN_KEY, false);
        expectedVelocityParams.put(DAYS_REMAINING_KEY, DAYS_REMAINING);
        expectedVelocityParams.put(OVERDUE_KEY, IS_OVERDUE);
        final Map<String, String> buildData = Collections.emptyMap();

        // Invoke and check
        assertVelocityParams(false, false, buildData, Collections.<String>emptyList(), expectedVelocityParams);
    }

    @Test
    public void panelShouldAddCorrectVelocityParamsWhenNonEmptyBuildDataIsAvailable() {
        // Set up
        final String buildResult = "someBuildResult";
        final String completedState = "someCompletedState";
        final Map<String, Object> expectedVelocityParams = new HashMap<String, Object>();
        expectedVelocityParams.put(BUILD_RESULT_KEY, buildResult);
        expectedVelocityParams.put(BUILD_TRIGGERED_KEY, true);
        expectedVelocityParams.put(COMPLETED_STATE_KEY, completedState);
        expectedVelocityParams.put(HAS_APPLINKS_KEY, false);
        expectedVelocityParams.put(IS_PROJECT_ADMIN_KEY, false);
        expectedVelocityParams.put(DAYS_REMAINING_KEY, DAYS_REMAINING);
        expectedVelocityParams.put(OVERDUE_KEY, IS_OVERDUE);
        final Map<String, String> buildData =
                ImmutableMap.of(PS_BUILD_RESULT, buildResult, PS_BUILD_COMPLETED_STATE, completedState);

        // Invoke and check
        assertVelocityParams(false, false, buildData, Collections.<String>emptyList(), expectedVelocityParams);
    }

    private void assertVelocityParams(
            final boolean bambooAppLinked, final boolean permissionToRelease, final Map<String, String> buildData,
            final List<String> releaseErrors, final Map<String, Object> expectedVelocityParams) {
        // Set up
        setUpBambooAppLinked(bambooAppLinked);
        setUpPermissionToRelease(permissionToRelease);
        when(bambooReleaseService.getBuildData(PROJECT_KEY, VERSION_ID)).thenReturn(buildData);
        when(releaseErrorReportingService.getErrors(PROJECT_KEY, VERSION_ID)).thenReturn(releaseErrors);

        // Invoke
        final Map<String, Object> velocityParams = bambooPanelHelper.getReleaseButtonParams(applicationUser, version, project);

        // Check
        assertThat(velocityParams, is(expectedVelocityParams));
        // -- This business logic is not ideal, see BDEV-5994
        // verify(bambooReleaseService).resetReleaseStateIfVersionWasUnreleased(version);
    }

    private void setUpBambooAppLinked(final boolean linked) {
        when(bambooApplicationLinkManager.hasApplicationLinks(UNFILTERED)).thenReturn(linked);
        when(permissionManager.hasPermission(VIEW_DEV_TOOLS, project, applicationUser)).thenReturn(true);
        when(globalPermissionManager.hasPermission(GlobalPermissionKey.SYSTEM_ADMIN, applicationUser)).thenReturn(true);
    }

    private void setUpPermissionToRelease(final boolean hasPermission) {
        when(bambooReleaseService.hasPermissionToRelease(applicationUser, project)).thenReturn(hasPermission);
    }
}
