package it.com.atlassian.jira.plugin.ext.bamboo.ctk;

import com.atlassian.federation.server.Server;

import javax.annotation.Nonnull;
import java.util.Properties;

import static com.atlassian.fusion.test.RestTestUtils.loadTestResource;
import static javax.ws.rs.core.MediaType.APPLICATION_JSON_TYPE;

/**
 * A stub Bamboo server that test cases can set up to respond to a given request in a given way.
 */
public class BambooCtkServer extends CtkServer {
    // The dev-status plugin queries this URL to see whether 2LO is enabled with Bamboo
    private static final String FUSION_2LO_CHECK_URL = "/rest/remote-link-aggregation/latest/aggregation?globalId=RAND-1";

    /**
     * Factory method for a server running locally on the given port.
     *
     * @param port the TCP/IP port the server should use
     * @return a non-started instance
     */
    @Nonnull
    public static BambooCtkServer getLocalInstance(final int port) {
        return new BambooCtkServer(new Server.Config("localhost", port));
    }

    public BambooCtkServer(@Nonnull final Server.Config config) {
        super(config);
    }

    /**
     * Sets this server's response to capabilities requests.
     *
     * @param capabilitiesJsonTemplateFile the name of the file containing the response; this
     *                                     method will substitute the server's base URL into it as required
     */
    public void setCapabilities(@Nonnull final String capabilitiesJsonTemplateFile) {
        final String capabilities = loadTestResource(capabilitiesJsonTemplateFile, getCapabilitySubstitutions());
        setResource("/rest/capabilities", capabilities, APPLICATION_JSON_TYPE);
    }

    /**
     * Sets this server's response to requests for the deployment status of the given issue.
     *
     * @param issueKey     the issue key
     * @param jsonResponse the JSON response to give out
     */
    public void setIssueDeploymentStatus(@Nonnull final String issueKey, @Nonnull final String jsonResponse) {
        setResource("/rest/api/latest/deploy/issue-status/" + issueKey, jsonResponse, APPLICATION_JSON_TYPE);
    }

    /**
     * Sets up this server to respond in a way that indicates two-legged OAuth (2LO) with this server is enabled.
     */
    public void set2LoEnabled() {
        // Any non-error response is sufficient for the purpose of simulating 2LO being enabled
        setResource(FUSION_2LO_CHECK_URL, "", APPLICATION_JSON_TYPE);
    }

    /**
     * Returns the substitutions to make into the capabilities response made by this server.
     *
     * @return a populated properties instance
     */
    private Properties getCapabilitySubstitutions() {
        final Properties properties = new Properties();
        // TODO Use the server's own base URL, not the URL of server 1?
        // Logged as https://extranet.atlassian.com/jira/browse/BDEV-6575
        properties.put("BASE_URL", "http://localhost:8990" /*getBaseUrl()*/);
        return properties;
    }
}
