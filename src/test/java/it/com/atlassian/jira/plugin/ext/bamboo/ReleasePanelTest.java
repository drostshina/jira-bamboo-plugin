package it.com.atlassian.jira.plugin.ext.bamboo;

import com.atlassian.jira.config.properties.APKeys;
import com.atlassian.jira.pageobjects.JiraTestedProduct;
import com.atlassian.jira.plugin.ext.bamboo.model.BuildState;
import com.atlassian.jira.testkit.client.Backdoor;
import com.atlassian.jira.testkit.client.util.TestKitLocalEnvironmentData;
import com.atlassian.pageobjects.TestedProductFactory;
import com.atlassian.pageobjects.elements.PageElement;
import com.atlassian.pageobjects.elements.timeout.TimeoutType;
import com.atlassian.webdriver.testing.rule.WebDriverScreenshotRule;
import it.com.atlassian.jira.plugin.ext.bamboo.pageobjects.BambooBrowseVersionPage;
import it.com.atlassian.jira.plugin.ext.bamboo.pageobjects.ReleaseDialog;
import it.com.atlassian.jira.plugin.ext.bamboo.pageobjects.ReleaseManagementSection;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Rule;
import org.junit.Test;

import java.util.List;

import static it.com.atlassian.jira.plugin.ext.bamboo.utils.PrivacyPolicyAcknowledgeControl.acknowledgePrivacyPolicy;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@Ignore("Flaky, see https://extranet.atlassian.com/jira/browse/BDEV-7127")
public class ReleasePanelTest {
    private static final String VERSION_1 = "1.0";
    private static final String VERSION_1_1 = "1.1";
    private static final String RELEASE_REPORT_DARK_FEATURE = "jira.plugin.devstatus.phasefour.enabled";

    @Rule
    public WebDriverScreenshotRule screenshotRule = new WebDriverScreenshotRule();

    private static JiraTestedProduct jira = TestedProductFactory.create(JiraTestedProduct.class,
            JiraProductInstance.INSTANCE, null);

    private Backdoor backdoor;
    private String projectKey;

    @Before
    public void setup() {
        backdoor = new Backdoor(new TestKitLocalEnvironmentData());

        jira.gotoLoginPage().loginAndGoToHome("admin", "admin");
        acknowledgePrivacyPolicy(jira);

        projectKey = WebDriverUtils.getNewProjectKey();
        backdoor.project().addProject("Test " + projectKey, projectKey, "admin");
        backdoor.applicationProperties().setString(APKeys.JIRA_BASEURL, jira.getProductInstance().getBaseUrl());

        WebDriverVersionUtils.createNewVersion(jira, projectKey, VERSION_1);
        backdoor.darkFeatures().enableForSite(RELEASE_REPORT_DARK_FEATURE);
    }

    @After
    public void tearDown() {
        backdoor.project().deleteProject(projectKey);
        backdoor.darkFeatures().disableForSite(RELEASE_REPORT_DARK_FEATURE);
    }

    @Test
    public void testReleasePanelIsVisible() throws Exception {
        BambooBrowseVersionPage browseVersions = WebDriverVersionUtils.goToBrowseVersionPageFor(jira, projectKey, VERSION_1);
        assertTrue("Browse versions page should have release tab", browseVersions.hasTab(ReleaseManagementSection.class));
    }

    @Test
    public void testCanReleaseWithSuccessfulBuild() throws Exception {
        try {
            final BuildState stateForVersion1 = openReleasePanelFor(VERSION_1).executeNewRelease("AP-SUCCEED");
            assertEquals(BuildState.SUCCESS, stateForVersion1);
            WebDriverVersionUtils.assertVersionReleased(jira, projectKey, VERSION_1);
        } catch (Throwable e) {
            System.out.println(jira.getTester().getDriver().getPageSource());
            throw new RuntimeException(e);
        }
    }

    @Test
    public void testCanNotReleaseIfNewBuildFails() throws Exception {
        final BuildState stateForVersion1 = openReleasePanelFor(VERSION_1).executeNewRelease("AP-FAIL");
        assertEquals(BuildState.FAILED, stateForVersion1);
        WebDriverVersionUtils.assertVersionNotReleased(jira, projectKey, VERSION_1);
    }

    @Test
    public void testVersionNameIsHtmlEncodedInReleasePanel() throws Exception {
        final String xssAttempt = "<SCRIPT>alert(\"version\")</SCRIPT>";
        WebDriverVersionUtils.createNewVersion(jira, projectKey, xssAttempt);
        BambooBrowseVersionPage page = WebDriverVersionUtils.goToBrowseVersionPageFor(jira, projectKey, xssAttempt);
        page.openTab(ReleaseManagementSection.class);
        Assert.assertFalse(jira.getTester().getDriver().getPageSource().contains(xssAttempt));
    }

    @Test
    public void testCanReleaseWithExistingBuild() throws Exception {
        try {
            WebDriverVersionUtils.createNewVersion(jira, projectKey, VERSION_1_1);

            final BuildState stateForVersion1 = openReleasePanelFor(VERSION_1).executeNewRelease("AP-SUCCEED");
            assertEquals(BuildState.SUCCESS, stateForVersion1);

            final BuildState stateForVersion11 = runFirstAvailableExistingBuildForRelease(VERSION_1_1, "AP-SUCCEED");
            assertEquals(BuildState.SUCCESS, stateForVersion11);

            WebDriverVersionUtils.assertVersionReleased(jira, projectKey, VERSION_1);
            WebDriverVersionUtils.assertVersionReleased(jira, projectKey, VERSION_1_1);
        } catch (Throwable e) {
            System.out.println(jira.getTester().getDriver().getPageSource());
            throw new RuntimeException(e);
        }
    }

    private BuildState runFirstAvailableExistingBuildForRelease(String version, String planKey, String... stages) {
        final ReleaseManagementSection releaseManagementSection = openReleasePanelFor(version);
        final ReleaseDialog dialog = releaseManagementSection.openReleaseDialog(true).selectExistingBuild(planKey);
        final List<PageElement> builds = dialog.allExistingBuilds();
        assertFalse("Must have at least one existing build", builds.isEmpty());
        builds.get(0).select();
        dialog.unselectAllStages().selectStages(stages).submit(true);
        return releaseManagementSection.waitForBuildToFinish(TimeoutType.PAGE_LOAD).getBuildStatus();
    }

    private ReleaseManagementSection openReleasePanelFor(String versionName) {
        return WebDriverVersionUtils.goToBrowseVersionPageFor(jira, projectKey, versionName).openTab(ReleaseManagementSection.class);
    }

}
