package it.com.atlassian.jira.plugin.ext.bamboo.pagecomponents.ual;

import com.atlassian.pageobjects.PageBinder;
import com.atlassian.webdriver.utils.element.WebDriverPoller;
import org.apache.axis.utils.StringUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import javax.inject.Inject;

import static com.atlassian.webdriver.utils.element.ElementConditions.isVisible;
import static it.com.atlassian.jira.plugin.ext.bamboo.WebDriverUtils.clickFirstVisibleElement;

public class ReciprocalUrlScreen {
    @FindBy(css = "#add-application-link-dialog #reciprocal-link-back-to-server")
    private WebElement reciprocalCheckbox;
    @FindBy(css = "#add-application-link-dialog #reciprocal-link-username")
    private WebElement reciprocalUsernameEl;
    @FindBy(css = "#add-application-link-dialog #reciprocal-link-password")
    private WebElement reciprocalPasswordEl;
    @FindBy(css = "#add-application-link-dialog #reciprocal-rpc-url")
    private WebElement reciprocalUrlEl;

    @Inject
    private PageBinder pageBinder;
    @Inject
    private WebDriver driver;
    @Inject
    private WebDriverPoller poller;

    public UsersAndTrustScreen submitReciprocalDetails(final boolean reciprocalLink, final String reciprocalUrl,
                                                       final String reciprocalUser, final String reciprocalPassword) {
        if (reciprocalLink) {
            reciprocalCheckbox.click();
            reciprocalUsernameEl.sendKeys(reciprocalUser);
            reciprocalPasswordEl.sendKeys(reciprocalPassword);
            if (!StringUtils.isEmpty(reciprocalUrl)) {
                reciprocalUrlEl.sendKeys(reciprocalUrl);
            }
        }

        clickFirstVisibleElement(driver, By.className("applinks-next-button"));

        // wait for next screen
        poller.waitUntil(isVisible(By.cssSelector("#add-application-link-dialog .step-3")));
        return pageBinder.bind(UsersAndTrustScreen.class);
    }
}
