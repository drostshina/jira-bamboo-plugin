package it.com.atlassian.jira.plugin.ext.bamboo.rest;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.util.List;

@JsonSerialize
public class VersionOperations {
    @JsonProperty
    private String name;

    @JsonProperty
    private List<Operation> operations;

    public static class Operation {
        @JsonProperty
        private String id;

        public String getId() {
            return id;
        }
    }

    public String getName() {
        return name;
    }

    public List<Operation> getOperations() {
        return operations;
    }

}
