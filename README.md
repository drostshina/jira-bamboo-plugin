# What Is It?

A JIRA plugin for integrating with Bamboo.

# Who Owns It?

The Bamboo team is responsible for doing releases, fixing bugs, etc.

# Resources

* Ask in the Bamboo room on HipChat
* See [EAC](https://extranet.atlassian.com/display/BAMBOO/Jira+Bamboo+Plugin) for more details.

# Builds

There are two separate build plans with different purposes:

* A build [on SerBac](https://server-gdn-bamboo.internal.atlassian.com/browse/PLUGINS-JBPT). runs verification tests, use for releasing
* A build [on Tardigrade](https://tardigrade-bamboo.internal.atlassian.com/browse/BAMPLUGINSCORE-JBAM) for testing JBAM with different branches of Bamboo. Do not create feature branches of this plan if you are making changes to JBAM.


URI.js was generated in http://medialize.github.io/URI.js/build.html with URI.js option only.

# Running

This project uses Maven 3 and therefore AMPS 5.x. For development purposes, you
can run it from the command line using `atlas-run` or `atlas-debug` as normal:

atlas-debug --instanceId jira

To run the CTK tests: atlas-debug --product jira-clean

To run JIRA : mvn jira:run -DskipITs

* To install artifact and run unit tests only:

mvn clean install -DskipITs

* To run unit tests only

mvn test